module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 50);
/******/ })
/************************************************************************/
/******/ ({

/***/ "1muL":
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("jgy0");


/***/ }),

/***/ "FiKB":
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "GL94":
/***/ (function(module, exports, __webpack_require__) {

const mongoose = __webpack_require__("FiKB");

const RegisterSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please add a title'],
    maxlength: [10, 'Title cannot be more than 40 characters']
  },
  name: {
    type: String,
    required: [true, 'Please add a title'],
    maxlength: [300, 'Title cannot be more than 40 characters']
  },
  email: {
    type: String,
    maxlength: [100, 'Title cannot be more than 40 characters']
  },
  tel: {
    type: String,
    maxlength: [10, 'Title cannot be more than 40 characters']
  },
  resumefile: {
    type: String,
    maxlength: [300, 'Title cannot be more than 40 characters']
  },
  type: {
    type: String,
    maxlength: [80, 'Title cannot be more than 40 characters']
  },
  branch: {
    type: String,
    maxlength: [80, 'Title cannot be more than 40 characters']
  },
  university: {
    type: String,
    maxlength: [80, 'Title cannot be more than 40 characters']
  },
  date: {
    type: String,
    maxlength: [50, 'Title cannot be more than 40 characters']
  }
});
module.exports = mongoose.model.Register || mongoose.model('Register', RegisterSchema);

/***/ }),

/***/ "jgy0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "mongoose"
var external_mongoose_ = __webpack_require__("FiKB");
var external_mongoose_default = /*#__PURE__*/__webpack_require__.n(external_mongoose_);

// CONCATENATED MODULE: ./utils/dbConnect.js

external_mongoose_default.a.set('useCreateIndex', true);
const connection = {};

async function dbConnect() {
  if (connection.isConnected) {
    return;
  }

  const db = await external_mongoose_default.a.connect("mongodb+srv://TT:adminTT@lineoa-tectony.6dj35.mongodb.net/logChat?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  connection.isConnected = db.connections[0].readyState;
  console.log(connection.isConnected);
}

/* harmony default export */ var utils_dbConnect = (dbConnect);
// EXTERNAL MODULE: ./models/Register.js
var Register = __webpack_require__("GL94");

// EXTERNAL MODULE: external "nodemailer"
var external_nodemailer_ = __webpack_require__("1muL");
var external_nodemailer_default = /*#__PURE__*/__webpack_require__.n(external_nodemailer_);

// CONCATENATED MODULE: ./pages/api/register/index.js



utils_dbConnect();
/* harmony default export */ var api_register = __webpack_exports__["default"] = (async (req, res) => {
  const {
    method
  } = req;

  switch (method) {
    case "POST":
      try {
        console.log(req.body.data);

        if (req.body.data.file_type == "pdf") {
          var file_name = req.body.data.f_name + "-" + req.body.data.l_name + ".pdf";
          var c_type = "application/pdf";
        } else {
          var file_name = req.body.data.f_name + "-" + req.body.data.l_name + ".docx";
          var c_type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        } // let item = {
        //   title: req.body.data.gender,
        //   name: req.body.data.f_name + " " + req.body.data.l_name,
        //   email: req.body.data.email,
        //   tel: req.body.data.phone,
        //   type: req.body.data.type,
        //   resumefile: req.body.data.path,
        //   branch: req.body.data.faculty,
        //   university: req.body.data.university,
        //   date: new Date(),
        // };
        // const register = await Register.create(item);


        let maildata = {
          from: "Website TecTony",
          to: "tectony@tectony.co.th,tan@tectony.co.yh,tan@tectony.co.th,yanisa@tectony.co.th",
          subject: "สมัครงานผ่าน Website TecTony",
          // text :"สมัครงานจาก : "+ req.body.data.gender + " " + req.body.data.f_name + " " + req.body.data.l_name+ "\n"+
          // "เบอร์ติดต่อ : " +  req.body.data.phone + "    อีเมล : " +  req.body.data.email,
          html: "<h3>สมัครงานจากคุณ : " + req.body.data.gender + " " + req.body.data.f_name + " " + req.body.data.l_name + "</h3>" + "<h3>ตำแหน่ง : " + req.body.data.type + "</h3>" + '<h3>เบอร์ติดต่อ : <a href="tel:' + req.body.data.phone + '">' + req.body.data.phone + "</a>  " + 'อีเมล : <a href="mailto:' + req.body.data.email + '">' + req.body.data.email + "</a></h3>",
          attachments: [{
            filename: file_name,
            path: "./public/resume/" + req.body.data.path,
            contentType: c_type
          }]
        };
        const smtpTransport = external_nodemailer_default.a.createTransport({
          service: "gmail",
          auth: {
            user: "reply-career@tectony.co.th",
            pass: "@aqhR**7C"
          }
        });
        smtpTransport.sendMail(maildata, function (error, response) {
          if (error) {
            console.log(error);
          } else {
            console.log("success mail send !");
          }

          smtpTransport.close();
        });
        res.status(200).json({
          success: true,
          data: register
        });
      } catch (error) {
        res.status(400).json({
          success: false
        });
      }

      break;

    default:
      res.status(400).json({
        success: false
      });
      break;
  }
});

/***/ })

/******/ });