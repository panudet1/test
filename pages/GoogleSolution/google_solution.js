import React from "react";

export default function google_solution() {
  return (
    <>
      <div className="container">
        <div className="row col-md-12">
          <div className="col-md-6">
            <img
              className=""
              src="/image/Graphic-design.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 mt-5">
            <span className="sup-digital-txt ">IT Solution</span>
            <ul>
              <div className="list-text mt-4">
                อัปเกรดระบบภายในธุรกิจของคุณด้วย Google Product ที่ทำให้คุณ
                ประหยัดค่าใช้จ่ายด้านซอฟต์แวร์ และประหยัดเวลาการทำงานระหว่างทีม
                ที่มีลูกค้าเป็นบริษัททั่วทุกมุมโลก
              </div>
            </ul>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row col-md-12 ">
          <div className="col-md-6">
            <img
              className=""
              src="/image/Graphic-design.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6">
            <span className="sup-digital-txt">
              <b>Graphic</b> Design
            </span>
            <ul>
              <div className="list-text mt-4">Motion Graphic</div>
              <div className="list-text mt-4">Banner/ AD โฆษณา</div>
              <div className="list-text mt-4">Infographic</div>
            </ul>
            <div className="mt-5">
              <button className="btn btn-tectony">คลิกดูรายละเอียด</button>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row col-md-12">
          <div className="col-md-6">
            <span className="sup-digital-txt">
              <b>Graphic</b> Design
            </span>
            <ul>
              <div className="list-text mt-4">Motion Graphic</div>
              <div className="list-text mt-4">Banner/ AD โฆษณา</div>
              <div className="list-text mt-4">Infographic</div>
            </ul>
            <div className="mt-5">
              <button className="btn btn-tectony">คลิกดูรายละเอียด</button>
            </div>
          </div>
          <div className="col-md-6">
            <img
              className=""
              src="/image/Graphic-design.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row col-md-12">
          <div className="col-md-6">
            <span className="sup-digital-txt">
              <b>Graphic</b> Design
            </span>
            <ul>
              <div className="list-text mt-4">Motion Graphic</div>
              <div className="list-text mt-4">Banner/ AD โฆษณา</div>
              <div className="list-text mt-4">Infographic</div>
            </ul>
            <div className="mt-5">
              <button className="btn btn-tectony">คลิกดูรายละเอียด</button>
            </div>
          </div>
          <div className="col-md-6">
            <img
              className=""
              src="/image/Graphic-design.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
    </>
  );
}
