import React from "react";
import App from "next/app";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-dom";
import "../public/css/style.css";
import "animate.css";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Head from "next/head";
// import "bootstrap-less";
export default class Home extends App {
  render() {
    const { Component } = this.props;
    // const img = "../public/image/Homepage.png";
    return (
      <div style={{ minHeight: "100vh" }}>
        <Head>
          <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
          <script src="https://unpkg.com/aframe-look-at-component@0.8.0/dist/aframe-look-at-component.min.js"></script>
          <script src="https://raw.githack.com/AR-js-org/AR.js/master/aframe/build/aframe-ar-nft.js"></script>
          <title>Welcome to Tectony</title>
          <link rel="icon" href="/image/Logo.png" />
        </Head>
        <div>
          <Header />
        </div>
        <div style={{ minHeight: "100vh" }}>
          <Component />
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}
