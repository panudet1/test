import React, { Component } from "react";
import axios, { post } from "axios";
import Modal from "react-modal";
import swal from "sweetalert";
export default class apply_job extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FileName: "เรซูเม่ PDF,DOCX",
      modalIsOpen: true,
      selectedFile: null,
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    };
  }

  CheckEmpty = () => {
    var that = this.state;
    if (that.gender === "คำนำหน้า") {
      swal("กรุณาระบุ", "คำนำหน้านาม", "warning");
    } else {
      if (that.f_name === "") {
        swal("กรุณาระบุ", "ชื่อ", "warning");
      } else {
        if (that.l_name === "") {
          swal("กรุณาระบุ", "นามสกุล", "warning");
        } else {
          if (that.email === "") {
            swal("กรุณาระบุ", "อีเมล์", "warning");
          } else {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(that.email)) {
              swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
            } else {
              if (that.phone === "") {
                swal("กรุณาระบุ", "เบอร์โทรศัพท์", "warning");
              } else {
                if (that.faculty === "") {
                  swal("กรุณาระบุ", "คณะ", "warning");
                } else {
                  if (that.university === "") {
                    swal("กรุณาระบุ", "มหาวิทยาลัย", "warning");
                  } else {
                    if (that.selectedFile === null) {
                      swal(
                        "กรุณาระบุอัพโหลดไฟล์",
                        "เรซูเม่ PDF,DOCX",
                        "warning"
                      );
                    } else {
                      this.Apply_job();
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  Apply_job = () => {
    var that = this.state;
    this.fileUpload(that.selectedFile).then((response) => {
      var path_resume = "";
      path_resume = response.data.files.file.path;

      if (path_resume.slice(-3) == "pdf") {
        var file_name = "pdf";
        var num = -43;
      } else {
        var file_name = "docx";
        var num = -44;
      }
      var data = {
        type: "Sales Manager",
        gender: that.gender,
        f_name: that.f_name,
        l_name: that.l_name,
        email: that.email,
        phone: that.phone,
        faculty: that.faculty,
        university: that.university,
        path: path_resume.slice(num),
        resume: path_resume.slice(num),
        file_type: file_name,
      };

      // console.log(data);
      axios({
        method: "post",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer my-token",
          "My-Custom-Header": "foobar",
        },
        url: "/api/register",
        data: JSON.stringify({ data: data }),
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
    });

    this.setState({
      FileName: "เรซูเม่ PDF,DOCX",
      position: "ตำแหน่งงาน",
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
      selectedFile: null,
    });
    swal("ส่งข้อมูลสำเร็จ", "", "success");
  };

  fileUpload = (file) => {
    const url = "/api/upload";
    const formData = new FormData();
    formData.append("file", file, file.name);
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };
    return axios.post(url, formData, config);
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };
  onChange = (e) => {
    if (e.target.files[0].name.slice(-3) == "pdf") {
      this.setState({
        FileName: e.target.files[0].name.slice(-13),
        selectedFile: e.target.files[0],
      });
    } else {
      if (e.target.files[0].name.slice(-4) == "docx") {
        this.setState({
          FileName: e.target.files[0].name.slice(-13),
          selectedFile: e.target.files[0],
        });
      } else {
        swal("ประเภทไฟลไม่ถูกต้อง", "กรุณาเลือกไฟล PDF,DOCX", "warning");
      }
    }
  };
  handleChangeInput = (event) => {
    const { value, maxLength } = event.target;
    const message = value.slice(0, maxLength);
    if (isNaN(message)) {
    } else {
      this.setState({
        phone: message,
      });
    }
  };
  render() {
    return (
      <>
        <div className="container" style={{ marginTop: "100px" }}></div>
        <div className="container">
          <form
            className="pay mb-3"
            style={{
              backgroundColor: "#68c7ff",
              padding: "20px",
              borderRadius: "19px",
            }}
          >
            <label
              style={{
                color: "white",
                fontFamily: "DBAdmanXBd",
                fontSize: "5vw",
              }}
            >
              ตำแหน่ง Sales Manager
            </label>
            <div className="form-row mb-3">
              <div className="col">
                <select
                  value={this.state.gender}
                  onChange={(e) => this.setState({ gender: e.target.value })}
                  id="inputState"
                  className="form-control"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                >
                  <option disabled selected>
                    คำนำหน้า
                  </option>
                  <option>นาย</option>
                  <option>นาง</option>
                  <option>นางสาว</option>
                </select>
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col md-5">
                <input
                  value={this.state.f_name}
                  onChange={(e) => this.setState({ f_name: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="ชื่อ"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
              <div className="col md-5">
                <input
                  value={this.state.l_name}
                  onChange={(e) => this.setState({ l_name: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="นามสกุล"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  type="email"
                  className="form-control"
                  placeholder="อีเมล"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.phone}
                  onChange={this.handleChangeInput}
                  type="text"
                  maxLength="10"
                  className=" form-control"
                  placeholder="เบอร์โทร"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.faculty}
                  onChange={(e) => this.setState({ faculty: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="คณะ"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
              <div className="col">
                <input
                  value={this.state.university}
                  onChange={(e) =>
                    this.setState({ university: e.target.value })
                  }
                  type="text"
                  className="form-control"
                  placeholder="มหาวิทยาลัย"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <div
                  className="form-control d-flex justify-content-between"
                  style={{
                    fontFamily: "DBAdmanXBd",
                    fontSize: "5vw",
                    color: "#328be0",
                    zIndex: "0",
                  }}
                >
                  {this.state.FileName}
                  <div className="upload-btn-wrapper ">
                    <button className="btn_upload_m">อัพโหลดไฟล์</button>
                    <input
                      type="file"
                      name="myfile"
                      accept=".pdf,.docx"
                      onChange={this.onChange}
                    />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="container mb-5">
          <button
            onClick={this.CheckEmpty}
            className="btn btn-contect mr-1 "
            style={{ fontSize: "5vw" }}
          >
            สมัครงาน
          </button>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={{ paddingTop: "100px", color: "#313131" }}
        >
          <div className="modal-header">
            <h5
              className="modal-title"
              style={{ fontSize: "30px", color: "#313131" }}
            >
              Sales Manager
            </h5>
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              data-dismiss="modal"
            >
              ×
            </button>
          </div>
          <div className="modal-body">
            <b>Jobs Descriptions</b>
            <ul>
              <li>
                {" "}
                Demonstrated ability leading sales planning and business
                operations or similar experience in technology or analytics
                functions, working across diverse teams that balanced between
                delivering on short-term revenue goals, while seeing around
                corners to enable future growth.
              </li>
              <li>
                {" "}
                Experience working for technology companies and in fast-growing
                sales environments.
              </li>
              <li>
                {" "}
                Proven track record in managing complex
                cross-functional/organizational initiatives concurrently.
              </li>
              <li>
                {" "}
                Adept at understand business financials and are able to
                translate and determine which drive investment decisions within
                the company or function.
              </li>
              <li>
                {" "}
                Deep understanding of the competitive landscape, industry
                trends, and external pressures that may impact customer buying
                decisions.
              </li>
              <li>
                {" "}
                Ability to handle difficult conversations and own the hard
                calls.
              </li>
              <li>
                {" "}
                You are someone who actively talks and listens through endless
                possibilities – with customers and teammates alike.
              </li>
              <li>
                {" "}
                You recognize and take onboard, perspectives, by listening and
                commit to what is right, in order to move forward.
              </li>
              <li>
                {" "}
                You are persistent and strive for excellence in everything you
                do.
              </li>
              <li>
                {" "}
                Ability to influence the right people, at all levels and
                functions in the company.
              </li>
              <li>
                {" "}
                You keep confidences and work selflessly to always do what’s
                best for TecTony.
              </li>
            </ul>
            <b>Qualifications</b>
            <ul>
              <li>
                You will drive sales initiatives, projects, analyses and you
                will lead a diverse team.
              </li>
              <li>
                You will govern, manage, and execute a portfolio of programs and
                projects.
              </li>
              <li>
                You will design and build a suite of dashboards that are
                consumable for management team.
              </li>
              <li>
                You will drive the development of “self-serve” analytics tools
                and data products.
              </li>
              <li>
                You will automate and scale existing analysis methods that
                advance modeling and analysis frameworks.
              </li>
            </ul>
            <hr />
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              data-dismiss="modal"
            >
              ลงทะเบียนสมัครงาน
            </button>
          </div>
        </Modal>
      </>
    );
  }
}
