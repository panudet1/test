import React, { Component } from "react";
import axios, { post } from "axios";
import Modal from "react-modal";
import swal from "sweetalert";
export default class apply_job extends Component {
  constructor(props) {
    super(props);
    this.state = {
      FileName: "เรซูเม่ PDF,DOCX",
      modalIsOpen: true,
      selectedFile: null,
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    };
  }

  CheckEmpty = () => {
    var that = this.state;
    if (that.gender === "คำนำหน้า") {
      swal("กรุณาระบุ", "คำนำหน้านาม", "warning");
    } else {
      if (that.f_name === "") {
        swal("กรุณาระบุ", "ชื่อ", "warning");
      } else {
        if (that.l_name === "") {
          swal("กรุณาระบุ", "นามสกุล", "warning");
        } else {
          if (that.email === "") {
            swal("กรุณาระบุ", "อีเมล์", "warning");
          } else {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(that.email)) {
              swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
            } else {
              if (that.phone === "") {
                swal("กรุณาระบุ", "เบอร์โทรศัพท์", "warning");
              } else {
                if (that.faculty === "") {
                  swal("กรุณาระบุ", "คณะ", "warning");
                } else {
                  if (that.university === "") {
                    swal("กรุณาระบุ", "มหาวิทยาลัย", "warning");
                  } else {
                    if (that.selectedFile === null) {
                      swal(
                        "กรุณาระบุอัพโหลดไฟล์",
                        "เรซูเม่ PDF,DOCX",
                        "warning"
                      );
                    } else {
                      this.Apply_job();
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  Apply_job = () => {
    var that = this.state;
    this.fileUpload(that.selectedFile).then((response) => {
      var path_resume = "";
      path_resume = response.data.files.file.path;

      if (path_resume.slice(-3) == "pdf") {
        var file_name = "pdf";
        var num = -43;
      } else {
        var file_name = "docx";
        var num = -44;
      }
      var data = {
        type: "Sales Engineer (Software)",
        gender: that.gender,
        f_name: that.f_name,
        l_name: that.l_name,
        email: that.email,
        phone: that.phone,
        faculty: that.faculty,
        university: that.university,
        path: path_resume.slice(num),
        resume: path_resume.slice(num),
        file_type: file_name,
      };

      // console.log(data);
      axios({
        method: "post",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer my-token",
          "My-Custom-Header": "foobar",
        },
        url: "/api/register",
        data: JSON.stringify({ data: data }),
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
    });

    this.setState({
      FileName: "เรซูเม่ PDF,DOCX",
      position: "ตำแหน่งงาน",
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
      selectedFile: null,
    });
    swal("ส่งข้อมูลสำเร็จ", "", "success");
  };

  fileUpload = (file) => {
    const url = "/api/upload";
    const formData = new FormData();
    formData.append("file", file, file.name);
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };
    return axios.post(url, formData, config);
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };
  onChange = (e) => {
    if (e.target.files[0].name.slice(-3) == "pdf") {
      this.setState({
        FileName: e.target.files[0].name,
        selectedFile: e.target.files[0],
      });
    } else {
      if (e.target.files[0].name.slice(-4) == "docx") {
        this.setState({
          FileName: e.target.files[0].name,
          selectedFile: e.target.files[0],
        });
      } else {
        swal("ประเภทไฟลไม่ถูกต้อง", "กรุณาเลือกไฟล PDF,DOCX", "warning");
      }
    }
  };
  handleChangeInput = (event) => {
    const { value, maxLength } = event.target;
    const message = value.slice(0, maxLength);
    if (isNaN(message)) {
    } else {
      this.setState({
        phone: message,
      });
    }
  };

  render() {
    return (
      <>
        <div className="container">
          <form
            className="pay mb-3"
            style={{
              backgroundColor: "#68c7ff",
              padding: "20px",
              borderRadius: "19px",
            }}
          >
            <label
              style={{
                color: "white",
                fontFamily: "DBAdmanXBd",
                fontSize: "36px",
              }}
            >
              ตำแหน่ง Sales Engineer (Software)
            </label>

            <div className="form-row mb-3">
              <div className="col">
                <select
                  value={this.state.gender}
                  onChange={(e) => this.setState({ gender: e.target.value })}
                  id="inputState"
                  className="form-control"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                >
                  <option disabled selected>
                    คำนำหน้า
                  </option>
                  <option>นาย</option>
                  <option>นาง</option>
                  <option>นางสาว</option>
                </select>
              </div>
              <div className="col md-5">
                <input
                  value={this.state.f_name}
                  onChange={(e) => this.setState({ f_name: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="ชื่อ"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
              <div className="col md-5">
                <input
                  value={this.state.l_name}
                  onChange={(e) => this.setState({ l_name: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="นามสกุล"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                  type="email"
                  className="form-control"
                  placeholder="อีเมล"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.phone}
                  onChange={this.handleChangeInput}
                  type="text"
                  maxLength="10"
                  className=" form-control"
                  placeholder="เบอร์โทร"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>

            <div className="form-row mb-3">
              <div className="col">
                <input
                  value={this.state.faculty}
                  onChange={(e) => this.setState({ faculty: e.target.value })}
                  type="text"
                  className="form-control"
                  placeholder="คณะ"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
              <div className="col">
                <input
                  value={this.state.university}
                  onChange={(e) =>
                    this.setState({ university: e.target.value })
                  }
                  type="text"
                  className="form-control"
                  placeholder="มหาวิทยาลัย"
                  style={{
                    borderWidth: "0",
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                  }}
                />
              </div>
            </div>
            <div className="form-row mb-3">
              <div className="col">
                <div
                  className="form-control d-flex justify-content-between"
                  style={{
                    fontFamily: "DBAdmanXBd",
                    fontSize: "24px",
                    color: "#328be0",
                    zIndex: "0",
                  }}
                >
                  {this.state.FileName}
                  <div className="upload-btn-wrapper   ">
                    <button className="btn_upload">อัพโหลดไฟล์</button>
                    <input
                      type="file"
                      name="myfile"
                      accept=".pdf,.docx"
                      onChange={this.onChange}
                    />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="container">
          <button
            onClick={this.CheckEmpty}
            className="btn btn-contect mr-1 "
            style={{ width: "250px" }}
          >
            สมัครงาน
          </button>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={{ color: "#313131" }}
        >
          <div className="modal-header">
            <h5
              className="modal-title"
              style={{ fontSize: "30px", color: "#313131" }}
            >
              Sales Engineer (Software)
            </h5>
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              data-dismiss="modal"
            >
              ×
            </button>
          </div>
          <div className="modal-body">
            <b>Jobs Descriptions</b>
            <ul>
              <li>
                {" "}
                ติดต่อนำเสนอการให้บริการ Software Solution ของบริษัทฯ
                ผ่านทางโทรศัพท์
              </li>
              <li> สร้างความสัมพันธ์อันดีระหว่างลูกค้ากับบริษัทฯ</li>
              <li> ทำยอดขายให้ได้ตามเป้าหมายในแต่ละเดือน</li>
              <li> บริหารจัดการรายชื่อลูกค้าที่มีเพื่อขยายฐานลูกค้าได้</li>
              <li>
                {" "}
                วิเคราะห์
                สรุปข้อมูลที่ได้จากการโทรหาลูกค้าเพื่อหาโอกาสในการปิดการขายได้
              </li>
              <li> ทำงานจันทร์-เสาร์ 9:00 - 18:00 น. (หยุดทุกวันอาทิตย์)</li>
              <li> วันหยุดนักขัตฤกษ์ ตามกำหนดของบริษัทฯ</li>
            </ul>
            <font color="red">
              *หากมีประสบการณ์ด้านการขายSoftware จะพิจารณาเป็นพิเศษ
            </font>
            <br />
            <b>Qualifications</b>
            <ul>
              <li>มีวินัยในตนเอง, อดทน, มีความรับผิดชอบ</li>
              <li>รักความก้าวหน้า, ชอบความท้าทาย</li>
              <li>ทนแรงกดดันในการทำงานได้ดี</li>
              <li>มีความสนใจในเรื่องไอทีและเทคโนโลยี ติดตามข่าวสารอยู่เสมอ</li>
              <li>มีใจรักการให้บริการ มองโลกในแง่ดี</li>
            </ul>
            <hr />
            <button
              onClick={this.closeModal}
              type="button"
              className="close"
              data-dismiss="modal"
            >
              ลงทะเบียนสมัครงาน
            </button>
          </div>
        </Modal>
      </>
    );
  }
}
