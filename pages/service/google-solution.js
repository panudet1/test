import { TextField , Grid , Paper } from '@material-ui/core';

const style = {
    textAlign: 'right',
  }

const GoogleSolution = () => (
    <>
        <div className="container main">
           {/* start google solutions */}
           <div className="google-solutions">
            <div className="row col-md-12">
                    <div className="col-md-6 mt-5">
                        <img className="img-marketing" src="/image/Google-solution-banner.jpg" width="140%" height="auto" alt="" />
                    </div>
                    <div className="col-md-6">
                        <span style={style} className="sup-digital-txt ml-5 "><b>Google</b> Solution</span>
                        <p style={style} className=" mr-3">
                            อัปเกรดระบบภายในธุรกิจของคุณด้วย Google <br/>
                            Product ที่ทำให้คุณ ประหยัดค่าใช้จ่ายด้าน<br/>
                            ซอฟต์แวร์ และประหยัดเวลาการทำงานระหว่างทีม<br/>
                            ที่มีลูกค้าเป็นบริษัททั่วทุกมุมโลก 
                        </p>
                    </div>
                    
                </div>
            </div>
            {/* end google solutions */}

            {/* start google solutions */}
            <div className="google-solutions">
                <div className="row col-md-12">
                    <div className="col-md-6">
                        <span className="sup-digital-txt"><b>Google G Suite</b></span>
                        <p className="mr-3">
                            ด้อยฟังก์ชั่นที่โดดเด่นของ Google G Suite <br/>
                            ที่ทีมคุณสามารถทำงานร่วมกันได้ทุกที่ทุกเวลาบ และสะดวกต่อการ<br/>
                            ใช้งาน เปลี่ยนให้ออฟฟิซของคุณเป็นโลกอันกว้างใหญ่<br/> 
                        </p>
                        <div className="mt-5"><button className="btn btn-tectony">คลิกดูรายละเอียด</button></div>
                    </div>
                    <div className="col-md-6">
                        <img className="img-fluid " src="/image/Graphic-design.png" width="120%" height="auto" alt="" />
                    </div>
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions">
            <div className="row col-md-12">
                    <div className="col-md-6 mt-5">
                        <img src="/image/Google_Drive_logo.png" width="85%" height="auto" alt="" />
                    </div>
                    <div className="col-md-6">
                        <div><span  className="head-title-gg-drive"><b>กูเกิลไดรฟ์</b></span><span className="head-sub-gg-drive">(Google Drive)</span></div>
                        
                        <div className="sub-title-gg-drive">Online File Storage Unlimited Lifetime</div>
                        <span className="old-price"><span className="old-price-txt">฿1,999</span></span>
                        <span className="new-price-txt">฿1,599</span>
                        <div className="title-detail-gg-drive">Google Drive Unlimited Storage & Lifetime</div>
                        <p className="des-detail-gg-drive mt-2">
                            Guarantee กู้บัญชีได้ เก็บข้อมูลได้ด้วยเนื้อที่ไม่จำกัด หลังจากที่ท่าน <br/>
                            ชำระค่าบริการเรียบร้อยแล้ว ทางทีมงาน TecTonyใช้เวลาดำเนินการ<br/>
                            ไม่เกิน 24 ชั่วโมงหลังจากที่รับทราบเมล์ แล้วท่านจะสามารถใช้งานได้ทันทีค่ะ
                        </p>
                        <div className="mt-2"><button className="btn btn-tectony">Add to cart</button><button className="btn btn-tectony ml-3">รายละเอียดเพิ่มเติม</button></div>
                    </div>
                    
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions mb-5">
            <div className="row col-md-12">
                <div className="col-md-8">
                        <div className="head-title-gg-drive">Google Workspace</div>
                        <div className="head-title-gg-wsp">ตลอดชีพ</div>

                        <div className="sub-title-gg-drive">แบบ Life Time ใช้ได้ตลอดชีพ ไม่มีค่าบริการรายเดือน</div>
                        <span className="new-price-txt">฿60,000.00 - ฿3,000,000.00</span>
                        <div className="title-detail-gg-drive">Google Drive Unlimited Storage & Lifetime</div>
                        <p className="des-detail-gg-drive mt-2">
                            บริการแอพพลิเคชั่นของ Google ซึ่งประกอบด้วย อีเมล์ ปฏิทิน ฮาร์ดไดรฟ์ออนไลน์ <br/>
                            ระบบจัดการเอกสาร ระบบแชท ฯลฯ ซึ่งแตกต่างจาก Google สำหรับบุคคลทั่วไป<br/>
                            ที่ใช้งานบน user โดเมน gmail.com โดยผู้ที่สมัครใช้บริการ Google Workspace <br/>
                            (Google Apps for Work) จะสามารถใช้งาน Google Apps <br/>
                            ได้เต็มรูปแบบ และสามารถใช้งานได้สะดวก รวดเร็ว ทุกสถานที่ทุกเวลา
                        </p>
                        <div className="mt-2"><button className="btn btn-line">Line @</button><button className="btn btn-tectony ml-3">รายละเอียดเพิ่มเติม</button></div>
                    </div>
                    <div className="col-md-4 mt-5">
                        <img src="/image/Marketing5.png" width="100%" height="auto" alt="" />
                    </div>
                    
                    
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions mb-5">
            <div className="row col-md-12">
                <div className="col-md-4">
                        
                    </div>
                    <div className="col-md-8 mt-5">
                        <div className="reply-mail-card">
                            <div className="reply-title-txt col-md-12">อยากให้เราติดต่อกลับ โปรดระบุอีเมลของคุณ</div>
                            <div className="row">
                                <div className="col-md-8">
                                    <input type="text" className="form-control input-blue p-white" placeholder="Enter yout email" />
                                </div>
                                <div className="col-md-3 mr-2">
                                    <button className="btn btn-white"><b>Send</b></button>
                                </div>    
                            </div>  
                        </div> 
                    </div>
                    
                    
                </div>
            </div>
            {/* end google solutions */}
        </div>
    </>
)

export default GoogleSolution