import { TextField , Grid , Paper } from '@material-ui/core';

const style = {
    textAlign: 'right',
  }

const DigitalMarketing = () => (
    <>
        <div className="container main">
           {/* start google solutions */}
           <div className="google-solutions">
            <div className="row col-md-12">
                    <div className="col-md-6 mt-5">
                        <img className="img-marketing" src="/image/Marketing2.png" width="140%" height="auto" alt="" />
                    </div>
                    <div className="col-md-6">
                        <p style={style} className="sup-digital-txt ml-5 "><b>Digital</b>Marketing</p>
                        <p style={style} className="">
                            Gold Opportunity Of Sales Channels<br/>
                            สร้างยอดขาย เพิ่มกำไรด้วยช่องทางการขายยอดนิยม<br/>
                            สร้างจุดแข็งที่เหนือกว่าคู่แข่งของแบรนด์<br/>
                            ลดต้นทุนและค่าใช้จ่ายต่างๆ เปิดธุรกิจให้บริการ<br/>
                            เพื่อสร้างยอดขายได้ตลอดเวลา 24/7
                        </p>
                    </div>
                    
                </div>
            </div>
            {/* end google solutions */}

            {/* start google solutions */}
            <div className="google-solutions">
                <div className="row col-md-12">
                    <div className="col-md-6">
                        <span className="sup-digital-txt"><b>รับจัดทำ Website</b></span>
                        <p className="mr-3">
                            รับจัดทำเว็บไซต์สำหรับองค์กร เว็บไซต์ทั่วไป เพื่อใช้เป็นสื่อกลางใน <br/>
                            การให้ข้อมูลต่างๆ และช่วยให้ลูกค้าเข้าถึงแบรนด์หรือธุรกิจของ<br/>
                            คุณได้ง่ายขึ้น รวมถึงเว็บไซต์สำหรับธุรกิจอีคอมเมิร์ซ<br/> 
                        </p>
                        <div className="mt-5"><button className="btn btn-tectony">คลิกดูรายละเอียด</button></div>
                    </div>
                    <div className="col-md-6">
                        <img className="img-fluid " src="/image/Website.png" width="120%" height="auto" alt="" />
                    </div>
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions">
            <div className="row col-md-12">
                    <div className="col-md-6 mt-5">
                        <img src="/image/digital-marketing.png" width="85%" height="auto" alt="" />
                    </div>
                    <div className="col-md-6">
                    <p style={style} className="sup-digital-txt ml-5 "><b>Line Marketing</b></p>
                        <p style={style} className="">
                            ช่วยให้คุณใกล้ชิดลูกค้ามากขึ้นด้วยสื่อ Line สร้างโอกาสขายและ<br/>
                            ความประทับใจให้ลูกค้าด้วยระบบทักทายอัตโนมัติ สื่อสารข้อมูล<br/>
                            หรือประชาสัมพันธ์โปรโมชั่นของคุณถึงลูกค้าในครั้งเดียว
                        </p>
                        <div style={style} className="red-price mt-2">เริ่มต้น 10,000 บาท</div>
                    </div>
                    
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions">
            <div className="row col-md-12">
                    
                    <div className="col-md-8">
                    <p className="sup-digital-txt left"><b>Facebook Marketing</b></p>
                        <p className="">
                            วางแผนการตลาดด้วยการใช้ Facebook สื่อโซเชียลสุดฮิตที่ทำให้<br/>
                            สินค้าและบริการของคุณเข้าถึงกลุ่มลูกค้าได้ง่ายที่สุด ทีมงานของ<br/>
                            เราจะช่วยวางแผนและวิเคราะห์การตลาดเพื่อสร้างรายได้และเติบโต<br/>
                            ตามเป้าหมายของคุณ
                        </p>
                        <div className="red-price mt-2 left">เริ่มต้น 30,000 บาท</div>
                    </div>
                    <div className="col-md-4 mt-5">
                        <img src="/image/Facebook-Marketing.png" width="100%" height="auto" alt="" />
                    </div>
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions mb-5">
            <div className="row col-md-12">
                <div className="col-md-4 mt-5">
                        <img src="/image/digital-marketing-2.jpg" width="100%" height="auto" alt="" />
                    </div>
                <div className="col-md-8">
                        <div className="head-title-gg-drive" style={style}>Graphic Design</div>
                        <div className="head-title-gg-wsp" style={style}>"น่าดู ดึงดูด ได้ใจ"</div>

                        <p className="" style={style}>
                            สร้างคอนเทนต์ที่ดี ด้วยสื่อที่หน้าสนใจ<br/>
                            สร้างความหลากหลาย และสะกดใจลูกค้าให้<br/>
                            กดสั่งซื้อสินค้า
                        </p>
                        <div className="mt-2"><button className="btn btn-tectony ml-3">Speak to our exports</button><button className="btn btn-line">Line @</button></div>
                    </div>
                    
                    
                    
                </div>
            </div>
            {/* end google solutions */}
            {/* start google solutions */}
           <div className="google-solutions mb-5">
            <div className="row col-md-12">
                <div className="col-md-4">
                        
                    </div>
                    <div className="col-md-8 mt-5">
                        <div className="reply-mail-card">
                            <div className="reply-title-txt col-md-12">อยากให้เราติดต่อกลับ โปรดระบุอีเมลของคุณ</div>
                            <div className="row">
                                <div className="col-md-8">
                                    <input type="text" className="form-control input-blue" placeholder="Enter yout email" />
                                </div>
                                <div className="col-md-3 mr-2">
                                    <button className="btn btn-white"><b>Send</b></button>
                                </div>    
                            </div>  
                        </div> 
                    </div>
                    
                    
                </div>
            </div>
            {/* end google solutions */}
        </div>
    </>
)

export default DigitalMarketing