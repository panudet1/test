import Link from "next/link";
import axios from "axios";
import { isMobile } from "react-device-detect";
import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import { ThemeProvider } from "react-bootstrap";
import swal from "sweetalert";
const text_sty1 = { fontSize: "30px", color: "#313131" };
const text_sty_mobile = { fontSize: "25px", color: "#313131" };
const font_mobile = { fontSize: "10vw", color: "#313131" };
const font_mobile_2 = { fontSize: "8vw", color: "#313131" };
const button_mobile = { fontSize: "20px" };
export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      subject: "",
      help: "",
    };
  }
  handleChangeInput = (event) => {
    const { value, maxLength } = event.target;
    const message = value.slice(0, 10);
    if (isNaN(message)) {
    } else {
      this.setState({
        phone: message,
      });
    }
  };
  handleSubmit = () => {
    var that = this.state;
    if (that.name === "") {
      swal("กรุณาระบุ", "ชื่อ", "warning");
    } else {
      if (that.email === "") {
        swal("กรุณาระบุ", "อีเมล", "warning");
      } else {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(that.email)) {
          swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
        } else {
          if (that.phone === "") {
            swal("กรุณาระบุ", "เบอร์โทรศัพท์", "warning");
          } else {
            if (that.subject === "") {
              swal("กรุณาระบุ", "หัวข้อ", "warning");
            } else {
              if (that.help === "") {
                swal("กรุณาระบุ", "ความต้องการ", "warning");
              } else {
                this.SendContact();
              }
            }
          }
        }
      }
    }
  };
  SendContact = () => {
    var that = this.state;
    axios({
      method: "post",
      url: "/api/helpmail",
      data: {
        name: that.name,
        email: that.email,
        phone: that.phone,
        subject: that.subject,
        canhelp: that.help,
      },
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    this.setState({
      name: "",
      email: "",
      phone: "",
      subject: "",
      help: "",
    });
    swal("ส่งข้อมูลสำเร็จ", "", "success");
  };

  renderContent = () => {
    if (isMobile) {
      return (
        <>
          <div className="container" style={{ marginTop: "0px" }}>
            <div className="row ml-3 mt-5">
              <h1 className="title-txt-banner mt-5" style={font_mobile}>
                IT SOLUTION & <br />
                DIGITAL MARKETING
              </h1>
            </div>
            <div className="row ml-3 mb-4">
              <span className="title-txt-sup" style={font_mobile_2}>
                Trust by Professional
              </span>
            </div>
            <div className="row ml-3 d-flex">
              <Link href="/contact_mobile">
                <button className="btn btn-contect" style={button_mobile}>
                  Contact Us
                </button>
              </Link>
            </div>
            <div className="row mt-5 d-flex justify-content-center">
              <div className="head-txt" style={{ fontSize: "40px" }}>
                Our Solutions
              </div>
            </div>
            <div className="row ml-1 mr-1 mt-4">
              <div className="mx-auto card-menu col d-flex align-items-center">
                <div className="row d-flex justify-content-center">
                  <div
                    className="solution-detail d-flex justify-content-center align-items-center"
                    style={{ fontSize: "25px" }}
                  >
                    Network Design
                    <a>
                      <img
                        src="/ICON SVG/network.svg"
                        width="40px"
                        height="40px"
                        alt=""
                        style={{ marginLeft: "10px" }}
                      />
                    </a>
                  </div>
                  <div
                    className=" d-flex justify-content-center ml-2 mr-2"
                    style={{ fontSize: "20px" }}
                  >
                    ออกแบบ Network Infrastructure เพื่อรองรับ เทคโนโลยี 5G และ
                    Multiple source ด้วยข้อมูล แบบ inside และ exclusive
                    ให้คุณได้ก้าวนำอยู่เสมอ ทั้งในการแข่งขัน ความคุ้มค่า
                    และการใช้งานได้เต็ม ประสิทธิภาพ
                  </div>
                </div>
              </div>
            </div>
            <div className="row ml-1 mr-1  mt-4">
              <div className="mx-auto card-menu col  d-flex align-items-center">
                <div className="row d-flex justify-content-center">
                  <div
                    className="solution-detail d-flex justify-content-center align-items-center"
                    style={{ fontSize: "25px" }}
                  >
                    Cloud Service
                    <a>
                      <img
                        src="/ICON SVG/cloud.svg"
                        width="50px"
                        height="50px"
                        alt=""
                        style={{ marginLeft: "10px" }}
                      />
                    </a>
                  </div>
                  <div
                    className=" d-flex justify-content-center   ml-2 mr-2"
                    style={{ fontSize: "20px" }}
                  >
                    แนะนำและให้บริการเชื่อมต่อโซลูชั่น Cloud และ Cloud Storage
                    (พื้นที่จัดเก็บข้อมูล) ที่ได้รับ อนุญาตสิทธิ์อย่างถูกต้องจาก
                    Google, Microsoft และ Amazon รับประกับตลอดอายุการใช้งาน
                    ภายใต้เงื่อนไขการให้บริการของผู้ให้บริการโดยตรง
                  </div>
                </div>
              </div>
            </div>
            <div className="row ml-1 mr-1 mt-4">
              <div className="mx-auto card-menu col  d-flex align-items-center">
                <div className="row d-flex justify-content-center">
                  <div
                    className="solution-detail d-flex justify-content-center align-items-center"
                    style={{ fontSize: "25px" }}
                  >
                    Cyber Security
                    <a>
                      <img
                        src="/ICON SVG/shield.svg"
                        width="50px"
                        height="50px"
                        alt=""
                        style={{ marginLeft: "10px" }}
                      />
                    </a>
                  </div>
                  <div
                    className=" d-flex justify-content-center  ml-2 mr-2"
                    style={{ fontSize: "20px" }}
                  >
                    ตรวจสอบ วิเคราะห์ แก้ไขช่องโหว่และวางแผน
                    ทางด้านความปลอดภัยของ Platform ตั้งแต่ระดับ Infrastructure
                    จนถึง User Interface และการใช้งานจริงของ User
                    คลอบคลุมทั้งฐานข้อมูล จุดเชื่อมต่อจนถึงพฤติกรรมการใช้งานของ
                    End User
                  </div>
                </div>
              </div>
            </div>
            <div className="row ml-1 mr-1  mt-4">
              <div className="mx-auto card-menu col d-flex align-items-center">
                <div className="row d-flex justify-content-center">
                  <div
                    className="solution-detail d-flex justify-content-center align-items-center"
                    style={{ fontSize: "25px" }}
                  >
                    Digital Marketing
                    <a>
                      <img
                        src="/ICON SVG/digital-campaign.svg"
                        width="50px"
                        height="50px"
                        alt=""
                        style={{ marginLeft: "10px" }}
                      />
                    </a>
                  </div>
                  <div
                    className=" d-flex justify-content-center  ml-2 mr-2"
                    style={{ fontSize: "20px" }}
                  >
                    ให้บริการจัดทำเว็บไซต์ วางแผน วิเคราะห์ การตลาด
                    และจัดทำสื่อออนไลน์เพื่อโปรโมต
                    ผ่านช่องทางออนไลน์เพื่อส่งเสริมการขาย
                    และโปรโมตแบรนด์และบริษัทให้เป็นที่รู้จัก
                    สร้างโอกาสให้ธุรกิจของคุณ
                  </div>
                </div>
              </div>
            </div>
            <div className="container ">
              <div className="row">
                <div className="col-md-12 align-self-center mt-5">
                  <img
                    className=""
                    src="/image/ItSolution/19197640.png"
                    width="100%"
                    height="auto"
                    alt=""
                    style={{ marginLeft: "10px" }}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 align-self-center mt-5">
                  <span
                    className="sup-digital-txt"
                    style={{ fontSize: "40px", color: "#313131" }}
                  >
                    <b>IT Soulotion</b>
                  </span>
                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/1046.png"
                      width="50px"
                      height="50px"
                      alt=""
                      style={{ marginLeft: "10px" }}
                    />
                    Software House
                  </div>
                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/3169209.png"
                      width="50px"
                      height="50px"
                      alt=""
                      style={{ marginLeft: "10px" }}
                    />
                    Data Warehouse
                  </div>
                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/17828.png"
                      width="50px"
                      height="50px"
                      alt=""
                      style={{ marginLeft: "10px" }}
                    />
                    Network Design
                  </div>

                  <div className="mt-4">
                    <Link href="/ITsolution/ITsolution_mobile">
                      <button
                        className="btn btn-tectony"
                        style={(button_mobile, { width: "150px" })}
                      >
                        คลิกดูรายละเอียด
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="container ">
              <div className="row">
                <div className="col-md-12 align-self-center mt-5">
                  <img
                    src="/image/DigitalMar/Digital Marketing.png"
                    width="100%"
                    height="auto"
                    alt=""
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 align-self-center mt-5">
                  <span
                    className="sup-digital-txt"
                    style={{ fontSize: "40px", color: "#313131" }}
                  >
                    <b>Digital</b> Marketing
                  </span>

                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/DigitalMar/Vector-Marketing.png"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    รับทำเว็ปไซต์
                  </div>
                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/DigitalMar/Facebook Marketing.svg"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Facebook Marketing
                  </div>
                  <div style={text_sty_mobile}>
                    <img
                      className="mr-5"
                      src="/image/DigitalMar/Line Marketing.svg"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Line Marketing
                  </div>

                  <div className="mt-4">
                    <Link href="/DigitalMarketing/digital_main_mobile">
                      <button
                        className="btn btn-tectony"
                        style={(button_mobile, { width: "150px" })}
                      >
                        คลิกดูรายละเอียด
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mt-5">
              <div className="head-txt" style={{ fontSize: "40px" }}>
                Work With Us
              </div>
              <div className="sub_header" style={{ fontSize: "25px" }}>
                Looking for help? Fill the form and start a new adventure.
              </div>
              <div className="col-md-12 mt-5 mb-5">
                <form className="" noValidate autoComplete="off">
                  <TextField
                    id="standard-basic"
                    label="Your name"
                    fullWidth
                    value={this.state.name}
                    onChange={(e) => this.setState({ name: e.target.value })}
                  />
                  <div className="row">
                    <div className="col-md-6">
                      <TextField
                        className="mt-4"
                        id="standard-basic"
                        label="Your email"
                        fullWidth
                        value={this.state.email}
                        onChange={(e) =>
                          this.setState({ email: e.target.value })
                        }
                      />
                    </div>
                    <div className="col-md-6">
                      <TextField
                        className="mt-4"
                        id="standard-basic"
                        label="Phone Number"
                        fullWidth
                        value={this.state.phone}
                        onChange={this.handleChangeInput}
                      />
                    </div>
                  </div>
                  <TextField
                    className="mt-4"
                    id="standard-basic"
                    label="Subject"
                    fullWidth
                    value={this.state.subject}
                    onChange={(e) => this.setState({ subject: e.target.value })}
                  />
                  <TextField
                    className="mt-4"
                    id="standard-basic"
                    label="How can we help?"
                    fullWidth
                    value={this.state.help}
                    onChange={(e) => this.setState({ help: e.target.value })}
                  />
                  <button
                    type="button"
                    onClick={this.handleSubmit}
                    className="btn btn-tectony mt-4"
                  >
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="container " style={{ marginTop: "50px" }}>
            <h1 className="title-txt-banner " style={{ fontSize: "6vw" }}>
              IT SOLUTION & <br />
              DIGITAL MARKETING
            </h1>
            <span className="title-txt-sup">Trust by Professional</span>
            <br />
            <Link href="/contact">
              <button className="btn btn-contect">Contact Us</button>
            </Link>
            <div className="container">
              <div className="head-txt" style={{ marginTop: "219px" }}>
                Our Solutions
              </div>
              <div>
                <div className="row col-md-12 mt-5">
                  <div className="mx-auto card-menu col-md-5  d-flex align-items-center pl-5 pr-5">
                    <div className="row d-flex justify-content-center">
                      <div className="solution-detail mb-3">
                        Network Design &nbsp;
                        <a>
                          <img
                            src="/ICON SVG/network.svg"
                            width="50px"
                            height="50px"
                            alt=""
                          />
                        </a>
                      </div>
                      ออกแบบ Network Infrastructure เพื่อรองรับ เทคโนโลยี 5G และ
                      Multiple source ด้วยข้อมูล แบบ inside และ exclusive
                      ให้คุณได้ก้าวนำอยู่เสมอ ทั้งในการแข่งขัน ความคุ้มค่า
                      และการใช้งานได้เต็ม ประสิทธิภาพ
                    </div>
                  </div>
                  <div className="mx-auto card-menu col-md-5  d-flex align-items-center pl-5 pr-5">
                    <div className="row d-flex justify-content-center">
                      <div className="solution-detail  mb-3">
                        Cloud Service &nbsp;
                        <a>
                          <img
                            src="/ICON SVG/cloud.svg"
                            width="50px"
                            height="50px"
                            alt=""
                          />
                        </a>
                      </div>
                      แนะนำและให้บริการเชื่อมต่อโซลูชั่น Cloud และ Cloud Storage
                      (พื้นที่จัดเก็บข้อมูล) ที่ได้รับ
                      อนุญาตสิทธิ์อย่างถูกต้องจาก Google, Microsoft และ Amazon
                      รับประกับตลอดอายุการใช้งาน
                      ภายใต้เงื่อนไขการให้บริการของผู้ให้บริการโดยตรง
                    </div>
                  </div>
                </div>

                <div className="row col-md-12 mt-5">
                  <div className="mx-auto card-menu col-md-5  d-flex align-items-center pl-5 pr-5">
                    <div className="row d-flex justify-content-center">
                      <div className="solution-detail  mb-3">
                        Cyber Security &nbsp;
                        <a>
                          <img
                            src="/ICON SVG/shield.svg"
                            width="50px"
                            height="50px"
                            alt=""
                          />
                        </a>
                      </div>
                      ตรวจสอบ วิเคราะห์ แก้ไขช่องโหว่และวางแผน
                      ทางด้านความปลอดภัยของ Platform ตั้งแต่ระดับ Infrastructure
                      จนถึง User Interface และการใช้งานจริงของ User
                      คลอบคลุมทั้งฐานข้อมูล
                      จุดเชื่อมต่อจนถึงพฤติกรรมการใช้งานของ End User
                    </div>
                  </div>
                  <div className="mx-auto card-menu col-md-5  d-flex align-items-center pl-5 pr-5">
                    <div className="row d-flex justify-content-center ">
                      <div className="solution-detail  mb-3">
                        Digital Marketing &nbsp;
                        <a>
                          <img
                            src="/ICON SVG/digital-campaign.svg"
                            width="50px"
                            height="50px"
                            alt=""
                          />
                        </a>
                      </div>
                      ให้บริการจัดทำเว็บไซต์ วางแผน วิเคราะห์ การตลาด
                      และจัดทำสื่อออนไลน์เพื่อโปรโมต
                      ผ่านช่องทางออนไลน์เพื่อส่งเสริมการขาย
                      และโปรโมตแบรนด์และบริษัทให้เป็นที่รู้จัก
                      สร้างโอกาสให้ธุรกิจของคุณ
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="container" style={{ marginTop: "411px" }}>
              <div className="row">
                <div className="col-md-6 align-self-center">
                  <span className="sup-digital-txt">
                    <b>IT Solution</b>
                  </span>
                  <div style={text_sty1}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/1046.png"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Software House
                  </div>
                  <div style={text_sty1}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/3169209.png"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Data Warehouse
                  </div>
                  <div style={text_sty1}>
                    <img
                      className="mr-5"
                      src="/image/ItSolution/17828.png"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Network Design
                  </div>
                  <div className="mt-4">
                    <Link href="/ITsolution/ITsolution">
                      <button className="btn btn-tectony">
                        คลิกดูรายละเอียด
                      </button>
                    </Link>
                  </div>
                </div>
                <div className="col-md-6 align-self-center">
                  <img
                    className=""
                    src="/image/ItSolution/19197640.png"
                    width="100%"
                    height="auto"
                    alt=""
                  />
                </div>
              </div>
            </div>

            <div className="container " style={{ marginTop: "232px" }}>
              <div className="row">
                <div className="col-md-6 align-self-center">
                  <img
                    src="/image/DigitalMar/Digital Marketing.png"
                    width="100%"
                    height="auto"
                    alt=""
                  />
                </div>
                <div className="col-md-6 align-self-center">
                  <span className="sup-digital-txt">
                    <b> Digital Marketing</b>
                  </span>

                  <div style={text_sty1}>
                    <img
                      className="mr-5 ml-5"
                      src="/image/DigitalMar/Vector-Marketing.png"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    รับทำเว็ปไซต์
                  </div>
                  <div style={text_sty1}>
                    <img
                      className="mr-5 ml-5"
                      src="/image/DigitalMar/Facebook Marketing.svg"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Facebook Marketing
                  </div>
                  <div style={text_sty1}>
                    <img
                      className="mr-5 ml-5"
                      src="/image/DigitalMar/Line Marketing.svg"
                      width="50px"
                      height="50px"
                      alt=""
                    />
                    Line Marketing
                  </div>
                  <div className="mt-4">
                    <Link href="/DigitalMarketing/digital_main">
                      <button className="btn btn-tectony">
                        คลิกดูรายละเอียด
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>

            <div className="google-solutions">
              <div className="head-txt">Work With Us</div>
              <div className="sub_header">
                Looking for help? Fill the form and start a new adventure.
              </div>
              <div className="col-md-12 mt-5 mb-5">
                <form className="" noValidate autoComplete="off">
                  <TextField
                    id="standard-basic"
                    label="Your name"
                    fullWidth
                    value={this.state.name}
                    onChange={(e) => this.setState({ name: e.target.value })}
                  />
                  <div className="row">
                    <div className="col-md-6">
                      <TextField
                        className="mt-4"
                        id="standard-basic"
                        label="Your email"
                        fullWidth
                        value={this.state.email}
                        onChange={(e) =>
                          this.setState({ email: e.target.value })
                        }
                      />
                    </div>
                    <div className="col-md-6">
                      <TextField
                        className="mt-4"
                        label="Phone Number"
                        fullWidth
                        value={this.state.phone}
                        onChange={this.handleChangeInput}
                      />
                    </div>
                  </div>
                  <TextField
                    className="mt-4"
                    id="standard-basic"
                    label="Subject"
                    fullWidth
                    value={this.state.subject}
                    onChange={(e) => this.setState({ subject: e.target.value })}
                  />
                  <TextField
                    className="mt-4"
                    id="standard-basic"
                    label="How can we help?"
                    fullWidth
                    value={this.state.help}
                    onChange={(e) => this.setState({ help: e.target.value })}
                  />
                  <button
                    type="button"
                    onClick={this.handleSubmit}
                    className="btn btn-tectony mt-4"
                  >
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </>
      );
    }
  };

  render() {
    return this.renderContent();
  }
}
