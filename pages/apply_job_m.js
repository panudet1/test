import React, { Component } from "react";

export default class apply_job extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    };
  }

  Apply_job = () => {
    // alert("ข้อมูลการสมัครงานถูกส่งเรียบร้อยแล้ว");
    // alert(this.state.value);
    // this.setState({ value: "คำนำหน้า" });
    var that = this.state;
    var data = [
      { gender: that.gender },
      { f_name: that.f_name },
      { l_name: that.l_name },
      { email: that.email },
      { phone: that.phone },
      { faculty: that.faculty },
      { university: that.university },
    ];
    console.log(data);

    // const requestOptions = {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: "Bearer my-token",
    //     "My-Custom-Header": "foobar",
    //   },
    //   body: JSON.stringify({ data: data }),
    // };
    // fetch("https://jsonplaceholder.typicode.com/posts", requestOptions)
    //   .then((response) => response.json())
    //   .then((data) => this.setState({ postId: data.id }));

    this.setState({
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    });
  };

  render() {
    return (
      <>
        <div className="container" style={{ marginTop: "100px" }}></div>
        <div className="container  mt-3">
          <div className="row">
            <div className="col">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/Web-WebApplicationDeveloper.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Web/Web Application Developer</p>
                  <a href="/Job/WebDev_m" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container  mt-3">
          <div className="row">
            <div className="col">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/SalesManager.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Sales Mamager</p>
                  <a href="/Job/Sale_m" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container  mt-3">
          <div className="row">
            <div className="col">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/Telesales.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Telesales (พนักงานขายทางโทรศัพท์)</p>
                  <a href="/Job/Tele_m" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container mt-3 mb-5">
          <div className="row">
            <div className="col">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/SalesEngineer.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Sales Engineer (Software)</p>
                  <a href="/Job/SaleEn_m" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
