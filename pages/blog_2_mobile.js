import React, { Component } from "react";
import Link from "next/link";
import swal from "sweetalert";
import axios from "axios";
export default class blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  handleSubmit = () => {
    var that = this.state;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(that.email)) {
      swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
    } else {
      axios({
        method: "post",
        url: "/api/sendmail",
        data: {
          email: that.email,
        },
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
      this.setState({
        email: "",
      });
      swal("ส่งข้อมูลสำเร็จ", "", "success");
    }
  };
  render() {
    return (
      <>
        <div className="container " style={{ marginTop: "100px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className="animate__animated animate__flipInX ">
                <Link href="/Blog/DataAnalytics_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/xData-Analytics.jpg" height="100%" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Data Analytics
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/BigData_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Big-data.jpg" height="100%" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Big Data
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Robot_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/Robot-vs-Ai-400x268.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Robot ต่างจาก AI อย่างไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Wifi2020_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Wifi.jpg" height="100%" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        WiFi ปี 2020 จะรวดเร็วมากขึ้นกว่าเดิมจริงหรือ?
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/5G2020_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/5G-Tecnology-600x402.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        เทคโนโลยี 5G ในปี 2020 จะมีอะไรเปลี่ยนไป?
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Cloud_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/01_AW_Cloud.jpg" height="100%" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Cloud มีความสำคัญอย่างไร ?!!
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div
          className="container"
          style={{
            marginTop: "50px",
            width: "100%",
            height: "100%",
            marginBottom: "20px",
          }}
        >
          <div
            className="d-flex flex-column  w-70 p-3"
            style={{
              backgroundColor: " #78bdff",
              height: "auto",
              borderRadius: "50px",
            }}
          >
            <div style={{ fontSize: "20px", fontFamily: "DBAdmanXBd" }}>
              อยากให้เราติดต่อกลับโปรดระบุอีเมลล์ของคุณ
            </div>
            <form className="form-inline">
              <div
                className="form-group mb-2"
                style={{
                  backgroundColor: "rgba(255, 255, 255, 0.22)",
                  marginRight: "10px",
                  borderRadius: "50px",
                  paddingLeft: "10px",
                  width: "200px",
                }}
              >
                <input
                  style={{
                    fontSize: "20px",
                    color: "white",
                    fontFamily: "DBAdmanX",
                  }}
                  type="email"
                  className="form-control-plaintext"
                  id="staticEmail2"
                  placeholder="Enter your email"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </div>
              <button
                type="button"
                className="btn btn-primary mb-2"
                onClick={this.handleSubmit}
                style={{
                  fontSize: "20px",
                  marginRight: "10px",
                  borderRadius: "50px",
                  borderWidth: "0px",
                  paddingLeft: "10px",
                  width: "100px",
                  backgroundColor: " #ffffff",
                  color: "black",
                }}
              >
                Send
              </button>
            </form>
          </div>
        </div>
        <div className="container mb-3 d-flex justify-content-end">
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item">
                <a className="page-link" href="./blog_mobile">
                  Previous
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_mobile">
                  1
                </a>
              </li>
              <li className="page-item active">
                <a className="page-link" href="./blog_2_mobile">
                  2
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_3_mobile">
                  3
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_3_mobile">
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}
