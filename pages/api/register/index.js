import dbConnect from "../../../utils/dbConnect";
import Register from "../../../models/Register";
import mailer from "nodemailer";

dbConnect();

export default async (req, res) => {
  const { method } = req;

  switch (method) {
    case "POST":
      try {
        console.log(req.body.data);
        if (req.body.data.file_type == "pdf") {
          var file_name =
            req.body.data.f_name + "-" + req.body.data.l_name + ".pdf";
          var c_type = "application/pdf";
        } else {
          var file_name =
            req.body.data.f_name + "-" + req.body.data.l_name + ".docx";
          var c_type =
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }
        // let item = {
        //   title: req.body.data.gender,
        //   name: req.body.data.f_name + " " + req.body.data.l_name,
        //   email: req.body.data.email,
        //   tel: req.body.data.phone,
        //   type: req.body.data.type,
        //   resumefile: req.body.data.path,
        //   branch: req.body.data.faculty,
        //   university: req.body.data.university,
        //   date: new Date(),
        // };
        // const register = await Register.create(item);
        let maildata = {
          from: "Website TecTony",
          to:
            "tectony@tectony.co.th,tan@tectony.co.yh,tan@tectony.co.th,yanisa@tectony.co.th",
          subject: "สมัครงานผ่าน Website TecTony",
          // text :"สมัครงานจาก : "+ req.body.data.gender + " " + req.body.data.f_name + " " + req.body.data.l_name+ "\n"+
          // "เบอร์ติดต่อ : " +  req.body.data.phone + "    อีเมล : " +  req.body.data.email,
          html:
            "<h3>สมัครงานจากคุณ : " +
            req.body.data.gender +
            " " +
            req.body.data.f_name +
            " " +
            req.body.data.l_name +
            "</h3>" +
            "<h3>ตำแหน่ง : " +
            req.body.data.type +
            "</h3>" +
            '<h3>เบอร์ติดต่อ : <a href="tel:' +
            req.body.data.phone +
            '">' +
            req.body.data.phone +
            "</a>  " +
            'อีเมล : <a href="mailto:' +
            req.body.data.email +
            '">' +
            req.body.data.email +
            "</a></h3>",
          attachments: [
            {
              filename: file_name,
              path: "./public/resume/" + req.body.data.path,
              contentType: c_type,
            },
          ],
        };
        const smtpTransport = mailer.createTransport({
          service: "gmail",
          auth: {
            user: "reply-career@tectony.co.th",
            pass: "@aqhR**7C",
          },
        });
        smtpTransport.sendMail(maildata, function (error, response) {
          if (error) {
            console.log(error);
          } else {
            console.log("success mail send !");
          }
          smtpTransport.close();
        });

        res.status(200).json({ success: true, data: register });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;

    default:
      res.status(400).json({ success: false });
      break;
  }
};
