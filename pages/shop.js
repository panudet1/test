import React from "react";
import Link from "next/link";
import { BrowserView, MobileView } from "react-device-detect";
export default function shop() {
  return (
    <>
      <MobileView>
        <div style={{ height: "10px" }}></div>
        <div className="container" style={{ marginTop: "200px" }}>
          <table id="cart" className="table">
            <thead style={{ backgroundColor: "#328be0", color: "white" }}>
              <tr className="names">
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Amount</th>
                <th />
              </tr>
            </thead>
            {/* <tbody>
              <tr className="item">
                <td data-th="Product">
                  <div className="row">
                    <div className="col-sm-3 hidden-xs">
                      <img
                        src="/image/G-Suite Lifetime License.png"
                        alt="Converse"
                        className="img-product img-responsive img-rounded"
                        width="100%"
                        height="auto"
                      />
                    </div>
                    <div className="col-sm-9">
                      <h4 className="nomargin">ตัวอย่างสินค้า</h4>
                      <p className="hidden-xs">รายละเอี่ยด</p>
                    </div>
                  </div>
                </td>
                <td data-th="Price" className="text-right hidden-xs">
                  $60,000.00
                </td>
                <td data-th="Quantity" className="text-right">
                  <div className="btn-group" role="group" aria-label="...">
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginRight: "10px",
                      }}
                    >
                      -
                    </button>
                    1
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginLeft: "10px",
                      }}
                    >
                      +
                    </button>
                  </div>
                </td>
                <td data-th="Subtotal" className="text-right">
                  $60,000.00
                </td>
                <td className="actions" data-th>
                  <button className="btn btn-default btn-sm">
                    <i className="fa fa-trash-o" />
                  </button>
                </td>
              </tr>
            </tbody> */}
            <tfoot>
              <tr>
                <td />
                <td className="hidden-xs" colSpan={2} />
                <td className="text-right">Total</td>
                <td className="text-right">
                  <strong> $60,000.00</strong>
                </td>
                <td />
              </tr>
              <tr className="no-border">
                <td>
                  <a href="#" className="btn btn-default ">
                    <i className="fa fa-angle-left" />
                  </a>
                </td>
                <td colSpan={2} className="hidden-xs" />
                <td />
                <td colSpan={2}>
                  <Link href="/pay">
                    <a
                      href="#"
                      className="btn btn-success btn-block"
                      style={{
                        backgroundColor: "#328be0",
                        borderWidth: "0px",
                      }}
                    >
                      Purchase <i className="fa fa-angle-right" />
                    </a>
                  </Link>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </MobileView>
      <BrowserView>
        <div className="container" style={{ display: "flex", height: "100vh" }}>
          <table id="cart" className="table table-responsive">
            <thead style={{ backgroundColor: "#328be0", color: "white" }}>
              <tr className="names">
                <th style={{ width: "100%" }}>Product</th>
                <th style={{ width: "10%" }} className="text-right hidden-xs">
                  Price
                </th>
                <th style={{ width: "15%" }} className="text-right">
                  Quantity
                </th>
                <th style={{ width: "50%" }} className="text-right">
                  Amount
                </th>
                <th style={{ width: "5%" }} />
              </tr>
            </thead>
            <tbody>
              <tr className="item">
                <td data-th="Product">
                  <div className="row">
                    <div className="col-sm-3 hidden-xs">
                      <img
                        src="/image/G-Suite Lifetime License.png"
                        alt="Converse"
                        className="img-product img-responsive img-rounded"
                        width="100%"
                        height="auto"
                      />
                    </div>
                    <div className="col-sm-9">
                      <h4 className="nomargin">ตัวอย่างสินค้า</h4>
                      <p className="hidden-xs">รายละเอี่ยด</p>
                    </div>
                  </div>
                </td>
                <td data-th="Price" className="text-right hidden-xs">
                  $60,000.00
                </td>
                <td data-th="Quantity" className="text-right">
                  <div className="btn-group" role="group" aria-label="...">
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginRight: "10px",
                      }}
                    >
                      -
                    </button>
                    1
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginLeft: "10px",
                      }}
                    >
                      +
                    </button>
                  </div>
                </td>
                <td data-th="Subtotal" className="text-right">
                  $60,000.00
                </td>
                <td className="actions" data-th>
                  <button className="btn btn-default btn-sm">
                    <i className="fa fa-trash-o" />
                  </button>
                </td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td />
                <td className="hidden-xs" colSpan={2} />
                <td className="text-right">Total</td>
                <td className="text-right">
                  <strong> $60,000.00</strong>
                </td>
                <td />
              </tr>
              <tr className="no-border">
                <td>
                  <a href="#" className="btn btn-default ">
                    <i className="fa fa-angle-left" />
                  </a>
                </td>
                <td colSpan={2} className="hidden-xs" />
                <td />
                <td colSpan={2}>
                  <Link href="/pay">
                    <a
                      href="#"
                      className="btn btn-success btn-block"
                      style={{
                        backgroundColor: "#328be0",
                        borderWidth: "0px",
                      }}
                    >
                      Purchase <i className="fa fa-angle-right" />
                    </a>
                  </Link>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </BrowserView>
    </>
  );
}
