import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import { BrowserView, MobileView } from "react-device-detect";
import Link from "next/link";
import swal from "sweetalert";
import axios from "axios";
export default class contact_mobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      subject: "",
      help: "",
    };
  }
  handleChangeInput = (event) => {
    const { value, maxLength } = event.target;
    const message = value.slice(0, 10);
    if (isNaN(message)) {
    } else {
      this.setState({
        phone: message,
      });
    }
  };
  handleSubmit = () => {
    var that = this.state;
    if (that.name === "") {
      swal("กรุณาระบุ", "ชื่อ", "warning");
    } else {
      if (that.email === "") {
        swal("กรุณาระบุ", "อีเมล", "warning");
      } else {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(that.email)) {
          swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
        } else {
          if (that.phone === "") {
            swal("กรุณาระบุ", "เบอร์โทรศัพท์", "warning");
          } else {
            if (that.subject === "") {
              swal("กรุณาระบุ", "หัวข้อ", "warning");
            } else {
              if (that.help === "") {
                swal("กรุณาระบุ", "ความต้องการ", "warning");
              } else {
                this.SendContact();
              }
            }
          }
        }
      }
    }
    // this.SendContact();
  };
  SendContact = () => {
    var that = this.state;
    axios({
      method: "post",
      url: "/api/helpmail",
      data: {
        name: that.name,
        email: that.email,
        phone: that.phone,
        subject: that.subject,
        canhelp: that.help,
      },
    }).then(
      (response) => {
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
    this.setState({
      name: "",
      email: "",
      phone: "",
      subject: "",
      help: "",
    });
    swal("ส่งข้อมูลสำเร็จ", "", "success");
  };
  render() {
    return (
      <>
        <div style={{ height: "50px" }}></div>
        <div className="container mt-5">
          <label
            className="head-txt-contact-m d-flex justify-content-center"
            style={{
              fontSize: "30px",
            }}
          >
            Get in touch
          </label>
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            Looking for help? Fill the form and start a new adventure.
          </label>
        </div>
        <div className="container">
          <div className="row">
            <div className="col d-flex justify-content-center ">
              <TextField
                value={this.state.name}
                id=""
                label="Your name"
                style={{ width: "100%" }}
                onChange={(e) => this.setState({ name: e.target.value })}
              />
            </div>
          </div>
          <div className="row">
            <div className="col d-flex justify-content-center">
              <TextField
                value={this.state.email}
                className="mt-4 "
                id=""
                label="Your email"
                style={{ width: "100%" }}
                onChange={(e) => this.setState({ email: e.target.value })}
              />
            </div>
          </div>
          <div className="row">
            <div className="col d-flex justify-content-center">
              <TextField
                value={this.state.phone}
                className="mt-4"
                id=""
                label="Phone Number"
                style={{ width: "100%" }}
                onChange={this.handleChangeInput}
              />
            </div>
          </div>
          <div className="row">
            <div className="col d-flex justify-content-center">
              <TextField
                value={this.state.subject}
                className="mt-4"
                id="standard-basic"
                label="Subject"
                style={{ width: "100%" }}
                onChange={(e) => this.setState({ subject: e.target.value })}
              />
            </div>
          </div>
          <div className="row">
            <div className="col d-flex justify-content-center">
              <TextField
                value={this.state.help}
                className="mt-4"
                id=""
                label="How can we help?"
                style={{ width: "100%" }}
                onChange={(e) => this.setState({ help: e.target.value })}
              />
            </div>
          </div>
        </div>

        <div className="container   mb-1 mt-5">
          <div className="row">
            <div className="col md-6 d-flex justify-content-center">
              <Link href="https://line.me/R/ti/p/%40yql4546f">
                <button className="btn btn-line-m">
                  Add Line@
                  <img
                    src="/ICON SVG/line-me.png"
                    width="25px"
                    height="25px"
                    alt=""
                  />
                </button>
              </Link>
            </div>
            <div className="col md-6 d-flex justify-content-center">
              <button onClick={this.handleSubmit} className="btn btn-submit-m">
                Submit
              </button>
            </div>
          </div>
        </div>

        <div className="container mt-5 ">
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            บริษัท เทคโทนี่ จำกัด
          </label>
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            119/984 หมู่ที่ 1 ตำบล ไทรม้า
          </label>
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            อำเภอ เมืองนนทบุรี จังหวัด นนทบุรี 11000
          </label>
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            โทรศัพท์ &nbsp;
            <a href="tel:+6686-416-4295"> (+66)86-416-4295 </a> ,&nbsp;
            <a href="tel:+662-077-2000"> (+66)2-077-2000 </a>
          </label>
          <label
            className="d-flex justify-content-center"
            style={{
              fontSize: "25px",
              fontWeight: "lighter",
              textAlign: "center",
              color: "#313131",
            }}
          >
            อีเมล์
            <Link href="mailto:info@tectony.co.th">
              <a className=" ml-1">info@tectony.co.th</a>
            </Link>
          </label>
        </div>
        <div className=" d-flex justify-content-center mb-5 mt-5">
          <iframe
            style={{ height: "50vh", width: "80%", maxWidth: "" }}
            src="https://maps.google.com/maps?q=13.888037,100.4770621&z=14&output=embed"
          />
        </div>
      </>
    );
  }
}
