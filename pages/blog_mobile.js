import React, { Component } from "react";
import Link from "next/link";
import swal from "sweetalert";
import axios from "axios";
export default class blog_mobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  handleSubmit = () => {
    var that = this.state;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(that.email)) {
      swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
    } else {
      axios({
        method: "post",
        url: "/api/sendmail",
        data: {
          email: that.email,
        },
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
      this.setState({
        email: "",
      });
      swal("ส่งข้อมูลสำเร็จ", "", "success");
    }
  };
  render() {
    return (
      <>
        <div className="container " style={{ marginTop: "100px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className="animate__animated animate__flipInX ">
                <Link href="/Blog/Phishing_email_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/Phishing-Email-600x402.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        ฟิชชิงอีเมล์ (Phishing Email) คืออะไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Email_server_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/Email-Server-600x600.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Email Server มีความสำคัญอย่างไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Work_from_home_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Hangout-Meet.jpg" height="100%" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        การทำงานแบบ Work From Home ด้วย Hangouts Meet
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Hangouts_meet_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/Google-Meet-800x800.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Hangouts Meet ประชุมงานได้ทุกที่
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/Word_press_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/WordPress-01-scaled.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        มารู้จักกับ WordPress เครื่องมือสร้างเว็บไซต์
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container " style={{ marginTop: "50px" }}>
          <div className="row d-flex justify-content-center ml-1 mr-2">
            <div className="col-md-12 ">
              <div className=" animate__animated animate__flipInX ">
                <Link href="/Blog/E_commerce_m">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img
                      src="./image/Blog/E-commerce-01-scaled.jpg"
                      height="100%"
                    />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        E-Commerce Website คืออะไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div
          className="container"
          style={{
            marginTop: "50px",
            width: "100%",
            height: "100%",
            marginBottom: "20px",
          }}
        >
          <div
            className="d-flex flex-column  w-70 p-3"
            style={{
              backgroundColor: " #78bdff",
              height: "auto",
              borderRadius: "50px",
            }}
          >
            <div style={{ fontSize: "20px", fontFamily: "DBAdmanXBd" }}>
              อยากให้เราติดต่อกลับโปรดระบุอีเมลล์ของคุณ
            </div>
            <form className="form-inline">
              <div
                className="form-group mb-2"
                style={{
                  backgroundColor: "rgba(255, 255, 255, 0.22)",
                  marginRight: "10px",
                  borderRadius: "50px",
                  paddingLeft: "10px",
                  width: "200px",
                }}
              >
                <input
                  style={{ color: "white", fontFamily: "DBAdmanX" }}
                  type="email"
                  className="form-control-plaintext"
                  id="staticEmail2"
                  placeholder="Enter your email"
                  value={this.state.email}
                  onChange={(e) => this.setState({ email: e.target.value })}
                />
              </div>
              <button
                type="button"
                className="btn btn-primary mb-2"
                onClick={this.handleSubmit}
                style={{
                  marginRight: "10px",
                  borderRadius: "50px",
                  borderWidth: "0px",
                  paddingLeft: "10px",
                  width: "100px",
                  backgroundColor: " #ffffff",
                  color: "black",
                }}
              >
                Send
              </button>
            </form>
          </div>
        </div>
        <div className="container mb-3 d-flex justify-content-end">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item active">
                <a class="page-link" href="./blog_mobile">
                  1
                </a>
              </li>
              <li class="page-item">
                <a class="page-link" href="./blog_2_mobile">
                  2
                </a>
              </li>
              <li class="page-item">
                <a class="page-link" href="./blog_3_mobile">
                  3
                </a>
              </li>
              <li class="page-item">
                <a class="page-link" href="./blog_2_mobile">
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}
