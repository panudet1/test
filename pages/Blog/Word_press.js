import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Word_press extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                มารู้จักกับ WordPress เครื่องมือสร้างเว็บไซต์
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ในช่วงหลายปีที่ผ่านมานี้ได้มีเครื่องมือที่ใช้ในการสร้างเว็บไซต์หลายๆตัวออกมา
                  ไม่ว่าจะเป็น Adobe Dreamwever , Web Page Maker , Kompozer
                  เป็นต้น แต่วันนี้พวกเราเทคโทนี่
                  จะพาเพื่อนๆไปทำความรู้จักกับเครื่องมือในการสร้างเว็บไซต์ที่เป็นที่นิยมที่สุดในขณะนี้เลยก็ว่าได้นั่นก็คือ
                  “WordPress”
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    WordPress คืออะไร ? มีข้อดีอะไร ?
                  </b>
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  1. WordPress Core เป็นซอฟแวร์หลัก ใช้จัดการเว็บไซต์
                  เนื้อหาและบทความต่างๆ
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  2. Theme เป็นส่วนที่กำหนดดีไซน์หรือรูปแบบการแสดงผล
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  3. Plugin เป็นส่วนที่ช่วยเพิ่มความสามารถให้กับ WordPress เช่น
                  ระบบสร้างหน้าเว็บไซต์,ระบบจัดการสินค้า เป็นต้น
                </div>
              </ul>
              <div className="list-text mt-4" style={text_detail}>
                เราก็รู้กันแล้วว่า WordPress คืออะไร คราวนี้เรามาดูกันว่า
                ข้อดีของ WordPress นั้นมีอะไรบ้าง
              </div>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  1. มีระบบจัดการบทความ หรือที่เรียกว่า “ระบบหลังบ้าน”
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  2. ประหยัดเวลาและค่าใช้จ่าย
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  3. มีดีไซน์หน้าเว็บให้เลือกหลากหลาย
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  4. อัพเดทข้อมูลได้ง่ายๆ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  เป็นยังไงกันบ้างครับเพื่อนๆกับเครื่องมือสร้างเว็บไซต์
                  “WordPress” ที่ทั้งประหยัดเวลา มีดีไซน์ให้เลือกมากมายแล้ว
                  ยังใช้งานได้ง่ายอีกด้วย
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
