import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                WiFi ปี 2020 จะรวดเร็วมากขึ้นกว่าเดิมจริงหรือ?
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แม้ว่า WiFi 6 และเทคโนโลยี 5G
                  นั้นเป็นเทคโนโลยีที่แตกต่างกันอย่างสิ้นเชิง แต่ในปี 2020 ทั้ง
                  Wifi 6 และ 5G จะนำเราไปสู่การประมวลผลที่เร็วขึ้น
                  และความเร็วในการเชื่อมต่อไร้สาย โดย 5G และ WiFi 6
                  ที่ทำงานแบบผสานจะสร้างการทำงานที่สมบูรณ์แบบ
                  สำหรับการเชื่อมต่อในบ้านและในสำนักงาน
                  คาดกันว่าความเร็วในการดาวน์โหลดจะสูงถึง 3 เท่า เร็วกว่าที่
                  WiFi 5 ทำได้
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แต่ประโยชน์ที่แท้จริงของ WiFi 6
                  คือความสามารถในการส่งข้อมูลได้รวดเร็วขึ้นไปยังอุปกรณ์ต่างๆได้มากกว่า
                  WiFi 5 โดยมีสองเหตุผลหลักที่สำคัญ
                  ประการแรกคือจำนวนอุปกรณ์ที่เชื่อมต่อในเครือข่าย WiFi
                  ที่คาดว่าจะเพิ่มขึ้นจาก 10 เป็น 50
                  ชิ้นโดยประมาณในอีกไม่กี่ปีข้างหน้า
                  และการเพิ่มขึ้นนั้นจะต้องเร็วมากยิ่งขึ้น มีประสิทธิภาพมากขึ้น
                  และความสามารถของ WiFi ที่มีความชาญฉลาดยิ่งขึ้น
                  เหตุผลที่สองคือคุณภาพและปริมาณของข้อมูลที่ใช้ผ่านเครือข่าย
                  WiFi ก็เพิ่มขึ้นเช่นกันและ WiFi 5
                  ก็ไม่สามารถรองรับการโหลดทั้งหมดได้ดีซึ่ง WiFi 6
                  จะกำจัดปัญหาเหล่านี้ให้หมดไปในปี 2020 นั่นเอง
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
