import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "40px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "20px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                Robot ต่างจาก AI อย่างไร
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  RPA (Robotic Process Automation) เรียกง่ายๆว่าคือ โรบอท หรือ
                  หุ่นยนต์นั่นเอง ทำหน้าที่คล้ายๆเลขาหรือผู้ช่วยของมนุษย์นั่นเอง
                  สิ่งที่ โรบอททำงานก็คือ
                  โรบอทจะทำงานตามคำสั่งที่ได้ถูกออกแบบจากมนุษย์
                  ไม่สามารถคิดเองทำเองได้ ทำให้ Robot ต่างจาก AI
                  ที่สามารถคิดและประมวลผลและตอบสนองการตัดสินใจได้
                  เพราะฉะนั้นในเวลาออกแบบโรบอทเพื่อช่วยงานของมนุษย์แล้ว
                  เราจะต้องมานั่งคุยถึงทุกเหตุการณ์ที่อาจะเกิดขึ้นได้
                  หรือที่เคยเกิดขึ้นมาแล้ว ยกตัวอย่างเช่น Error
                  ต่างๆที่ได้เกิดขึ้น หากพบ Error เรามีการจัดการอย่างไร
                  ทำอย่างไร
                  นำมาเก็บรวบรวมเป็นข้อมูลในการสร้างเส้นทางให้โรบอททำงานให้ดียิ่งขึ้นต่อไป
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ในอนาคตอันใกล้นี้ โรบอทจะเข้ามามีบทบาทในตลาดแรงงานเป็นอย่างมาก
                  การมองหาผู้เชี่ยวชาญในการพัฒนาโรบอทมาช่วยงานก็มีความสำคัญเช่นกัน
                  หากคุณกำลังมองหาที่ปรึกษาในเรื่องไอทีโซลูชั่น
                  เทคโทนี่ยินดีให้บริการ
                  เพราะเราเชื่อว่าทุกไอเดียของคุณเป็นจริงได้ ด้วย unlimited
                  possibilities ความเป็นไปได้ที่ไม่มีขีดจำกัดด้วย Digital
                  Solution จากเทคโทนี่
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
