import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "40px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "20px",
  textIndent: "30px",
};
export default class Email_server extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5" style={{ height: "50px" }}></div>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                Email Server มีความสำคัญอย่างไร
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แน่นอนว่าเรื่องของความน่าเชื่อถือและภาพลักษณ์ขององค์กรถือเป็นสิ่งสำคัญเป็นอย่างมาก
                  ไม่ว่าจะเป็นเรื่องงาน หรือ บุคลากรตลอดไปจนถึงเรื่องเล็กๆ
                  ที่หลายคนมองข้ามอย่างเรื่อง Email
                  หลายคนคงเริ่มตั้งคำถามแล้วว่า Email
                  จะเกี่ยวกับภาพลักษณ์ขององค์กรเรายังไง
                  หากคุณยังนึกภาพไม่ออกให้ลองนึกว่า
                  เวลาทำที่เราไปติดต่องานแล้วเจอองค์กรที่ใช้ Email
                  ที่พบเห็นทั่วไปกับองค์กรที่ใช้ชื่อ Domain ของ Email
                  เป็นชื่อเฉพาะขององค์กรอย่างเช่น name@yourbusiness.com
                  แน่นอนว่าแบบหลังจะให้ความรู้สึกที่เป็นมืออาชีพมากยิ่งกว่า
                  แต่หลายองค์กรก็ยังขาดความรู้ความเข้าใจว่าเราควรทำอย่างไรให้ได้มาซึ่ง
                  Email ตามที่เรากล่าวไป
                  และในบทความนี้เราจึงขออาสาพาทุกท่านมารู้จัก Email Server
                  กันว่ามีความสำคัญอย่างไร
                  และเราจะใช้ประโยชน์กับองค์กรในด้านใดได้บ้าง
                  และหากคุณพร้อมแล้วเรามารู้ไปพร้อมกันในบทความนี้เลย
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  อย่างที่ได้เกริ่นไว้ด้านบน หลายคนคงได้เห็นถึงความสำคัญของ
                  Email Server กันมาบ้างแล้ว แน่นอนว่าการที่เราจะทำ Server
                  ขึ้นมาเองนั้นก็คงเป็นเรื่องที่ยุ่งยากและใช้งบประมาณมากพอสมควรอีกทั้งยังต้องมาจัดการเรื่องของ
                  Security ด้วยตัวเอง เรียกได้ว่าวุ่นวายและใช้เงินอย่างมาก
                  แต่จะดีกว่าไหมถ้าหากเราเลือกใช้ Email Server
                  ชื่อดังที่เปิดให้บริการอยู่ในขณะนี้อย่าง G Suite ซึ่งข้อดีของ
                  G Suite สำหรับองค์กรคุณก็คือ
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    1. สามารถใช้ Domain ชื่อ Email เป็นชื่อขององค์กรเราได้ทันที
                    &nbsp;
                  </b>
                  ทุกคนในองค์กรเราสามารถใช้ชื่อ Email ของตนเองติดต่อกับ Partner
                  ได้เลยทันที เช่น name@yourbusiness.com
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    2. มีระบบ Security ที่ดี &nbsp;
                  </b>
                  ด้วยความที่เป็น Host ที่สร้างโดย Google
                  เราก็ไม่ต้องกังวลเรื่องความปลอดภัยเพราะมีทีมงานของ Google
                  ดูแลและเก็บข้อมูลไว้บน Server ที่ปลอดภัยเสมอ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    3. มีเครื่องมือและ UI ที่ทันสมัย &nbsp;
                  </b>
                  หมดกังวลเรื่องการใช้งาน เพราะ ด้วย Tool ต่างๆ จาก Google
                  ที่เราคุ้นเคยกันเป็นอย่างดี ทำให้ทุกคนสามารถใช้งานได้ง่าย
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    4. สามารถใช้เครื่องมือต่างๆ บน Google ได้ &nbsp;
                  </b>
                  สามารถกำหนดการนัดหมาย แชร์ข้อมูลผ่านเครื่องมือต่างๆ ของ google
                  ได้ เช่น ปฏิทิน ,Google Drive เป็นต้น
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    5. สร้างหรือส่งเอกสารผ่าน Google Docs, Sheets ได้ &nbsp;
                  </b>
                  สามารถใช้เครื่องมือของ G Suite
                  เพื่อสร้างส่งรับแชร์ไฟล์ในรูปแบบที่ง่ายและสามารถทำได้อย่างรวดเร็วทันใจ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ซึ่ง G Suite ก็จะช่วยคุณและองค์กรของคุณได้รับทั้งความสะดวก
                  และความปลอดภัย ทำให้สามารถใช้ email
                  ภายใต้ชื่อขององค์กรตัวเองไว้ใช้งาน ซึ่งการใช้งาน G Suite
                  ก็สามารถทำได้ง่ายๆ และ Google Workspace
                  ก็ยังคอยช่วยเหลือซัพพอร์ตแก้ปัญหาต่างๆ
                  ให้กับคุณได้ตลอดเวลาอีกด้วย ดังนั้นจึงไม่ควรลังเลที่จะให้ G
                  Suite เป็นตัวช่วยและคอยดูแล Email ให้กับคุณได้แล้ววันนี้
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
