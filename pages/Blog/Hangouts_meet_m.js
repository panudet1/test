import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "40px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "20px",
  textIndent: "30px",
};
export default class Hangouts_meet extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5" style={{ height: "50px" }}></div>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                Hangouts Meet ประชุมงานได้ทุกที่
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  มีเพื่อนๆคนไหนเจอสถาการณ์แบบนี้กันบ้างครับ?
                  ประชุมงานทีไรต้องเข้าออฟฟิศ รถก็ติด เดินทางทีนึงก็หลายชั่วโมง
                  วันนี้เทคโทนี่ มีการประชุมรูปแบบใหม่ที่จะทำให้เพื่อนๆ
                  อยู่ที่ไหนก็สามารถประชุม หรือนำเสนองานได้มาเล่าให้ฟัง ..
                  นั่นก็คือ
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">Google Hangouts Meet</b>
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  เป็น Google Apps ที่จะให้เพื่อนๆสามารถประชุมงาน นำเสนองาน ผ่าน
                  VDO Conference
                  ไม่ว่าจะอยู่ที่ไหนทุกมุมโลกก็สามารถประชุมงานกันได้
                  แล้วยังป้องกันการเข้าร่วมประชุมจากบุคคลภายนอกอีกด้วย
                  ในยุคที่เริ่มเข้าสู่ 5G
                  ก็สามารถทำให้เพื่อนๆสามารถทำงานที่ไหนก็ได้ ประชุมงาน
                  คุยงานกันที่ไหนก็ได้ ง่ายยิ่งกว่าปอกกล้วยเข้าปากอีกคร้าบ…
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
