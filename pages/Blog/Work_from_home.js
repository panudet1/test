import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#7c7c7c",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                การทำงานแบบ Work From Home ด้วย Hangouts Meet
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แน่นอนว่าในช่วงสถานการณ์การแพร่ระบาดของเชื้อไวรัสโคโรนา 2019
                  (โควิด 19/ Covid19) โรคระบาดที่เราทุกคนกำลังพบเจอเช่นนี้
                  ต่างก็สร้างความลำบากให้ต่อทุกหน่วยงานและภาคส่วน
                  และสถานการณ์เช่นนี้เองการเดินทางออกไปทำงานจึงเป็นเรื่องที่เสี่ยงอันตรายมากขึ้น
                  ด้วยเหตุผลนี้เองจึงทำให้หลายบริษัทมีนโนบายช่วยลดความเสี่ยงในเดินทางออกมาทำงานที่บริษัทของลูกจ้างโดยการ
                  &nbsp; Work From Home
                  แต่อย่างไรก็ดีหลายที่ก็ยังติดปัญหาเรื่องการเลือก Tool
                  ที่เหมาะสมที่จะนำมาใช้ในการติดต่อและประชุมงาน
                  ดังนั้นในบทความนี้เราจึงมีเครื่องมือตัวช่วยในการทำงานในสถานการณ์ที่ทุกคนควรทำงานที่บ้านอย่างเครื่องมือจากค่ายยักษ์ใหญ่
                  Google ที่มีชื่อว่า Hangouts Meet
                  เพื่อให้ทุกคนสามารถทำงานต่อเนื่องเหมือนกับนั่งทำอยู่ที่บริษัทยังไงงั้น
                  และเพื่อไม่ให้เป็นการเสียเวลาเราก็ไปรู้จักเครื่องมือนี้ไปพร้อมกันเลย
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ก่อนจะไปดูกันว่าทำไมต้องเลือกใช้ Hangouts Meet
                  เราก็ไปทำความรู้จักกันกับเครื่องมือนี้กันก่อน Hangouts Meet
                  เป็นเครื่องมือที่ช่วยให้เราสามารถติดต่อสื่อสารผ่านทางวิดีโอผ่านในระยะไกลได้
                  ดังนั้นเครื่องมือนี้จึงเป็นเครื่องมือที่ตอบโจทย์การทำงานแบบ
                  Work From Home ในยุคนี้เป็นอย่างมาก
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">ขั้นตอนการใช้งาน</b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  - เพียงคุณมี E-mail (Gmail / GSuite) ของ Google
                  ก็สามารถเข้าใช้งานได้ทันที
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - หากใช้บนคอมพิวเตอร์ก็สามารถเข้าไปยังเว็บไซต์
                  https://meet.google.com ได้เลยทันที หรือสามารถใช้งานบน Mobile
                  ได้ผ่านทาง Application Google Meet
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    ข้อดีของการใช้งาน Hangouts Meet
                  </b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  - รองรับการใช้งานการประชุมร่วมกันได้มากสุดถึง 250 คน/ครั้ง
                  ขึ้นอยู่กับแพ็กเกจ ซึ่งเหมาะแก่การประชุมงานร่วมกัน
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - ข้อมูลสำคัญอย่างภาพและเสียงมีการเข้ารหัสเพื่อความปลอดภัย
                  เหมาะแก่การส่งข้อมูลสำคัญอย่างข้อมูลงานหากัน
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - มี UI ที่สวยงามและใช้ง่าย
                  เหมาะกับผู้ที่เริ่มต้นใช้งานเครื่องออนไลน์
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - สามารถจัดตารางการประชุมหรือเวลางานต่าง ๆ
                  เพื่อแจ้งเตือนสมาชิกได้
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - สามารถบันทึก VDO การประชุมได้อีกด้วย
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - เป็นเครื่องมือที่เปิดให้ใช้งานได้ฟรีไม่เสียค่าใช้จ่าย
                  (Gmail)
                  หรือจะเลือกการใช้งานแบบพรีเมี่ยมในลักษณะการบริหารจัดการแบบองค์กรก็ได้
                  (GSUITE)
                </div>
                <div className="list-text mt-4" style={text_detail}>
                  - รองรับการทำงานได้หลากหลาย platform ทั้งอุปกรณ์คอมพิวเตอร์
                  โทรศัพท์มือถือ หรืออุปกรณ์สื่อสารอื่น ๆ
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  จะเห็นได้ว่าฟังก์ชันต่าง ๆ ที่มีมาให้ใน Hangouts Meet
                  ถือว่าเป็นเครื่องมือที่เหมาะกับเหตุการณ์ในปัจจุบันนี้ที่จะมาช่วยให้การทำงานที่บ้านสามารถทำได้สะดวกมากยิ่งขึ้น
                  และเป็นเครื่องมือที่สามารถเข้าถึงการใช้งานได้ง่าย
                  ใช้งานได้ง่าย สะดวก ทั้งยังมีการดูแลเรื่องความปลอดภัยของข้อมูล
                  เรียกได้ว่าเป็นอีกหนึ่งเครื่องมือที่เป็นตัวช่วยให้เราสามารถผ่านวิกฤตของโลกระบาดนี้ไปด้วยกัน
                  นอกจากนี้เองก็ยังเป็นก้าวสำคัญที่จะช่วยมาพัฒนาการ Work From
                  Home ให้กลายเป็นเรื่องปกติได้ในอนาคตอีกด้วย
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
