import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                G Suite คืออะไร?
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ในยุคที่{" "}
                  <b className="font-weight-bold">ปลาเร็วกินปลาใหญ่ &nbsp;</b>
                  SMEสามารถพัฒนาธุรกิจได้อย่างก้าวกระโดด 4G
                  เข้ามามีบทบาทมากขึ้นในชีวิตประจำวัน ไม่ว่าจะเป็นการทำงาน
                  ดูหนัง หรือฟังเพลง ทุกอย่างล้วนอยู่บน Cloud และต้องใช้ E-Mail
                  user เพื่อให้สามารถเข้าถึงข้อมูลดังกล่าวได้
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b className="font-weight-bold">
                    แล้วอะไรล่ะ? ที่ทำให้ SME
                    สามารถพัฒนาธุรกิจได้รวดเร็วขนาดนั้น
                  </b>
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  Platform ที่จะช่วยให้การทำงานของคุณมีประสิทธิภาพมากขึ้น
                  ไม่ว่าจะทำอะไร อยู่ที่ไหน ก็ไม่พลาดทุกการติดต่อสื่อสาร
                  ทำให้ทุกอย่างเป็นเรื่องง่ายแค่ปลายนิ้วs
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  Platform นั้นก็คือ <b className="font-weight-bold">G Suite</b>{" "}
                  นั่นเอง!!
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  G Suite เป็นระบบการให้บริการของ Google ในรูปแบบการทำงาน On
                  Cloud ซึ่งทำให้ผู้ใช้สามารถทำงานร่วมกันแบบออนไลน์ในรูปแบบใหม่
                  ที่ไม่ใช่แค่ใช้อีเมลและแชท แต่ยังมีการประชุม
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ทางวิดีโอ โซเชียลมีเดีย การทำงานร่วมกันในเอกสารแบบเรียลไทม์
                  นำเสนองานผ่าน Video Conference และอื่นๆอีกมากมาย
                  ไซส์ของธุรกิจไม่ใช่เรื่องสำคัญอีกต่อไป เพียงแค่มี Google
                  Workspace ทุกสิ่งก็สามารถเกิดขึ้นได้
                  จึงไม่น่าแปลกใจที่ปัจจุบัน “ปลาใหญ่อาจจะต้องโดนปลาเร็วกิน”
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
