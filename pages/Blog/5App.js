import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                รวม 5 แอพเด็ดสำหรับธุรกิจที่ต้อง Work From Home
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  หากพูดถึงGoogle คงไม่มีใครไม่รู้จัก
                  วันนี้น้องแอดได้รวบรวมแอพดีๆเพื่อให้ธุรกิจที่กำลังมีนโยบายจะ
                  Work From Home เพื่อเป็นข้อมูลเบื้องต้นในการวางแผนงานกันค่ะ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b class="font-weight-bold">1. Hangouts Meet</b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  เป็นแอพที่ใช้กันในรูปแบบ Video Conference จะทำให้คุณ Say Hi!
                  แบบเห็นหน้าเห็นตากัน
                  หรืออาจจะเป็นกันสนทนา/ประชุมกันในรูปแบบเสียงเพียงอย่างเดียวก็ได้
                  และในการสนทนานั้นสามารถโชว์หน้าจอเพียงนำเสนองานให้ผู้เข้าร่วมประชุมได้เห็นงานของตนได้อีกด้วย
                  สำหรับการ Work From Home ที่มีเวลาการทำงานที่ชัดเจนก็สามารถใช้
                  Hangouts Meet แทนการเช็คอินเวลาเข้าทำงาน
                  และทำงานอยู่ด้วยกันโมเม้นต์นี้เหมือนการนั่งทำงานอยู่ข้างๆกันเลยทีเดียว
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b class="font-weight-bold">2. Google Calendar</b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แอพที่ใช้สำหรับการทำตารางงาน
                  หรือเป็นปฏิทินออนไลน์ที่ออกแบบสำหรับทีมช่วยในการวางแผนการทำงาน
                  ทำงานร่วมกันได้ง่าย และสะดวกขึ้น
                  ทำให้คุณทราบแผนงานถัดไปของคุณเสมอ ถึงจะ Work From Home
                  ก็วางแผนการทำงานได้อย่างไม่สะดุดนะจ๊ะ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b class="font-weight-bold">
                    3. ทำงานเอกสารผ่านแอพ Google Docs (งานเอกสาร),
                    Sheet(งานคำนวณ), Slide(งานนำเสนอ)
                  </b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ความสุดยอดในแอพคือการทำงาน On Cloud
                  ที่สามารถทำงานร่วมกันได้พร้อมๆกันหลายๆท่านบนเอกสารเดียวกันโดยไม่ต้องส่งงานไปมาหากันเพื่อแก้งานที่สำคัญขอบอกว่า
                  Google Docs, Sheet, Slide นี้ Save Autoนะจ๊ะ ไฟดับ
                  ไฟตกงานก็ไม่หายนะจ๊ะ อันนี้ดีต่อใจสุดๆ แอดมินปลื้มมากกกก
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b class="font-weight-bold">4. Google Drive</b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แอพที่ช่วยในการจัดเก็บ เข้าถึง
                  แชร์ไฟล์ในที่เดียวได้อย่างปลอดภัย
                  และค้นหาสิ่งที่คุณต้องการได้ทันทีบนพื้นที่ On Cloud
                  อัพไฟล์ขึ้น Google Drive Work From Home ได้อย่างสบาย
                  กลับไปทำงานที่ Office ก็ทำงานต่อได้เลยนะคะ แม้ไฟล์จะใหญ่
                  Google Drive ก็ช่วยได้จ้าาา
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  <b class="font-weight-bold">5. Email</b>
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แอพที่จะช่วยให้คุณไม่พลาดการติดต่อสื่อสารกับลูกค้าภายนอกองค์กรและ
                  เพื่อนร่วมงานได้สะดวกสุดๆไปเลยจ๊ะพี่จ๋า
                  นี่เป็นเพียงแค่ส่วนหนึ่งเท่านั้นนะคะที่แอดมินคัดสรรมาให้
                  ซึ่งเป็นสิ่งที่มีประโยชน์มากสำหรับการ Work From Home
                  เทคโทนี่ขออยู่เคียงข้างทุกธุรกิจเพื่อผ่านวิกฤตนี้ไปด้วยกันค่ะ
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
