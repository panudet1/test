import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "40px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "20px",
  textIndent: "30px",
};
export default class E_commerce extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5" style={{ height: "50px" }}></div>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                E-Commerce Website คืออะไร
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ในช่วงหลายๆปีที่ผ่านมาเราจะได้ยินเกี่ยวกับ E-Commerce,
                  E-Service, E-Payment เป็นต้น แล้วเพื่อนๆรู้รึป่าวว่ามันคืออะไร
                  มันช่วยเราได้อย่างไร วันนี้เรา เทคโทนี่ จะพาเพื่อนๆไปรู้จักกับ
                  “เว็บไซต์ E-Commerce” ว่ามันคืออะไร แล้วมันสามารถทำอะไรได้
                  เว็บไซต์ E-Commerce คือ เว็บไซต์ที่จำหน่ายสินค้า
                  หรือบริการขายสินค้าผ่านทางระบบอินเตอร์เน็ต
                  ที่ลูกค้าสามารถเข้าไปดูและซื้อสินค้าได้เลยโดยตรง และในปัจจุบัน
                  ตลาด E-Commerce ได้เติมโตขึ้นมากทำให้มีเว็บไซต์ E-Commerce
                  มากขึ้นตามไปด้วย
                  นอกจากนี้ยังมีผู้ประกอบการที่นำมาใช้งานในธุรกิจ หรือ
                  ประยุกต์การใช้งานให้เข้ากับธุรกิจของตัวเอง ทำให้เกิด เว็บไซต์
                  E-Commerce ที่แตกต่างกันออกไป เป็นยังไงกันบ้างครับเพื่อนๆ
                  คราวนี้ก็รู้แล้ว่า เว็บไซต์ E-Commerce คืออะไร
                  แล้วทำอะไรได้บ้าง
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
