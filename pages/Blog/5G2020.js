import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                เทคโนโลยี 5G ในปี 2020 จะมีอะไรเปลี่ยนไป?
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ว่ากันว่าปี 2020 จะเป็นปีของเทคโนโลยี 5G
                  ที่มาพร้อมกับแบรนด์ใหญ่ๆทางด้านโทรคมนาคม
                  และผู้ผลิตโทรศัพท์มือถือก็ได้มีการปล่อยโทรศัพท์ที่รองรับเทคโนโลยี
                  5G ออกมาบ้างแล้ว โดยเทคโนโลยี 5G ในปี 2020
                  นั้นจะนำเสนอความเร็วของบรอดแบนด์ที่รวดเร็วมากขึ้น
                  ซึ่งการแพร่หลายของเทคโนโลยี 5G นี้
                  จะทำให้เกิดเทคโนโลยีอัจฉริยะอีกมากมาย
                  ไม่ว่าจะเป็นยานพาหนะอัจฉริยะ การผลิตอัจฉริยะ
                  และรวมถึงเทคโนโลยี IoT สำหรับ 5G กล่าวอีกนัยหนึ่งก็คือ
                  เทคโนโลยี 5G จะไม่ถูกจำกัดไว้เพียงแค่โทรศัพท์เท่านั้น
                  แต่ยังคงมีในทุกอุตสาหกรรมที่เราได้สัมผัสในชีวิตประจำวัน
                  ซึ่งจะถูกเปลี่ยนให้ดียิ่งขึ้นโดยวิวัฒนาการทางเทคโนโลยีในปี
                  2020 นั่นเอง
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ..
                  อีกไม่นานนี้คงได้เห็นนวัตกรรมใหม่ๆเกิดขึ้นให้พวกเราได้ตื่นตาตื่นใจแน่นอนครับ
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
