import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "80px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "25px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                Data Analytics
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ก่อนหน้านี้เราได้พูดคุยกันไปแล้วกับเรื่อง Big Data ว่าคืออะไร
                  วันนี้เราจะมาคุยกันต่อในเรื่อง Data Analytics หรือ
                  การวิเคราะห์ข้อมูล ว่ามันเชื่อมโยงกับ Big Data ได้ยังไง
                  แล้วสามารถนำมาต่อยอดเพื่อช่วยในด้านไหนได้บ้าง
                  เราไปทำความรู้จักกับ Data Analytics กันเลยครับ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  Data Analytics (การวิเคราะห์ข้อมูล) คือ การที่นำข้อมูลจาก Big
                  Data มาทำการวิเคราะห์ เพื่อใช้ประโยชน์ในเชิงธุรกิจ
                  หรือวัตถุประสงค์อื่นๆที่ตามที่เราต้องการ
                  เริ่มต้นโดยการนำข้อมูลจาก Big Data นั้นๆ
                  มาแปลงให้อยู่ในรูปแบบที่พร้อมจะประมวลผลโดยอาศัยชุดคำสั่งทางเทคโนโลยีและแบบจำลองที่สร้างขึ้น
                  แล้วนำข้อมูลที่ได้จากการวิเคราะห์นั้นมาใช้งาน
                  โดยรูปแบบของการวิเคราะห์ข้อมูลสามารถจำแนกได้ดังนี้
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  1.การวิเคราะห์ข้อมูลแบบพื้นฐาน (Descriptive analytics)
                  เป็นการวิเคราะห์เพื่อแสดงผลของรายการทางธุรกิจ เหตุการณ์
                  หรือกิจกรรมต่างๆ ที่ได้เกิดขึ้น
                  หรืออาจกำลังเกิดขึ้นในลักษณะที่ง่ายต่อการเข้าใจ
                  หรือต่อการตัดสินใจ เช่น รายงานการขาย รายงานผลการดำเนินงาน
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  2.การวิเคราะห์แบบเชิงวินิจฉัย (Diagnostic analytics)
                  เป็นการอธิบายถึงสาเหตุของสิ่งที่เกิดขึ้น ปัจจัยต่างๆ
                  และความสัมพันธ์ของปัจจัยหรือตัวแปรต่างๆ
                  ที่มีความสัมพันธ์ต่อกันของสิ่งที่เกิดขึ้น เช่น
                  ความสัมพันธ์ระหว่างยอดขายต่อกิจกรรมทางการตลาดแต่ละประเภท
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  3.การวิเคราะห์แบบพยากรณ์ (Predictive analytics)
                  เป็นการวิเคราะห์เพื่อพยากรณ์สิ่งที่กำลังจะเกิดขึ้นหรือน่าจะเกิดขึ้น
                  โดยใช้ข้อมูลที่ได้เกิดขึ้นแล้วกับแบบจำลองทางสถิติ หรือ
                  เทคโนโลยีปัญญาประดิษฐ์ต่างๆ (Artificial intelligence) เช่น
                  การพยากรณ์ยอดขาย
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  4.การวิเคราะห์แบบให้คำแนะนำ (Prescriptive analytics)
                  เป็นการวิเคราะห์ข้อมูลที่มีความซับซ้อนที่สุด
                  เป็นทั้งการพยากรณ์สิ่งต่างๆ ที่จะเกิดขึ้น ข้อดี ข้อเสีย สาเหตุ
                  และระยะเวลา ของสิ่งที่จะเกิดขึ้น
                  และการให้คำแนะนำทางเลือกต่างๆที่มีอยู่ และผลของแต่ละทางเลือก
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  เป็นยังไงกันบ้างครับ ทีนี้เพื่อนๆก็รู้กันแล้วว่า Big Data
                  สามารถนำไปทำอะไรได้บ้าง
                  แต่ถ้าเพื่อนๆคนไหนยังคิดไม่ออกหรือต้องการข้อมูลเพิ่มเติม
                  บอกเทคโทนี่ได้นะครับ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  “เราเชื่อเสมอว่าทุกไอเดียของคุณเป็นจริงได้ เพราะ unlimited
                  possibilities ความเป็นไปได้ที่ไม่มีขีดจำกัดด้วย Digital
                  Solution จากเทคโทนี่”
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
