import React, { Component } from "react";
const text_title = {
  color: "#313131",
  fontSize: "40px",
  fontFamily: "DBAdmanXBd",
};
const text_detail = {
  color: "#313131",
  fontSize: "20px",
  textIndent: "30px",
};
export default class Work_from_home extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5">
          <div className="row col-md-12">
            <div className="col align-self-center">
              <span className="sup-digital-txt" style={text_title}>
                Big Data
              </span>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  ในยุคที่ข้อมูลมีค่าดั่งน้ำมัน
                  และมีบทบาทเข้ามากำหนดทิศทางของธุรกิจและอื่นๆอีกมากมาย
                  วันนี้เทคโทนี่จะพาไปรู้จักกับสิ่งเหล่านี้ที่เรียกว่า “Big
                  Data” กันครับ
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  Big data คือ ข้อมูลที่เก็บรวบรวมไว้ป็นจำนวนมากๆ
                  หรือเรียกได้ว่า เป็นข้อมูลขนาดมหาศาลที่มีเป็นล้านๆหน่วยความจำ
                  โดยการเก็บข้อมูลให้เป็น Big Data นั้น
                  แต่ละหน่วยงานอาจจะมีการเก็บที่แตกต่างกันออกไป
                  ตามโครงสร้างของข้อมูล ดังนี้
                </div>
              </ul>

              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  1. ข้อมูลที่มีโครงสร้างชัดเจน (Structured Data) เช่น
                  ข้อมูลที่เก็บอยู่ในตารางข้อมูลต่างๆที่ถูกออกแบบมาไว้แล้ว
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  2. ข้อมูลกึ่งมีโครงสร้าง (Semi-Structured Data) เช่น ล็อกไฟล์
                  (Log Files)
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  3. ข้อมูลที่ไม่มีโครงสร้าง (Unstructured Data) เช่น
                  ข้อมูลการโต้ตอบปฏิสัมพันธ์ผ่าน โซเชี่ยลเน็ตเวิร์ก (Social
                  Network) ไม่ว่าจะเป็น Facebook, twitter หรือไฟล์จำพวกมีเดีย
                  เป็นต้น
                </div>
              </ul>
              <ul>
                <div className="list-text mt-4" style={text_detail}>
                  แต่ข้อมูลทั้งหมดนี้ยังเป็นเพียงข้อมูลดิบที่รอการนำมาประมวลผลและวิเคราะห์ทางเทคโนโลยี
                  เพื่อนำไปพัฒนาในด้านอื่นๆต่อไป หากเพื่อนๆมีข้อมูลที่เป็น Big
                  Data อยู่ในมืออยู่แล้ว
                  และอยากพัฒนาองค์ความรู้ที่มีให้เกิดประโยชน์ในเชิงธุรกิจอย่างสูงสุด
                  แต่ยังนึกไม่ออกว่าจะต้องทำอย่างไร .. #ขอเพียงส่งเสียงมาจะไปหา…
                  (เพลงพี่เบิร์ดก็มา ^^)
                </div>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
