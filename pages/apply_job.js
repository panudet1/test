import React, { Component } from "react";
import axios, { post } from "axios";

export default class apply_job extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    };
  }

  Apply_job = () => {
    // alert("ข้อมูลการสมัครงานถูกส่งเรียบร้อยแล้ว");
    // alert(this.state.value);
    // this.setState({ value: "คำนำหน้า" });
    var that = this.state;
    var data = [
      { gender: that.gender },
      { f_name: that.f_name },
      { l_name: that.l_name },
      { email: that.email },
      { phone: that.phone },
      { faculty: that.faculty },
      { university: that.university },
    ];
    console.log(data);
    // const requestOptions = {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //     Authorization: "Bearer my-token",
    //     "My-Custom-Header": "foobar",
    //   },
    //   body: JSON.stringify({ data: data }),
    // };
    // fetch("https://jsonplaceholder.typicode.com/posts", requestOptions)
    //   .then((response) => response.json())
    //   .then((data) => this.setState({ postId: data.id }));
    this.setState({
      // position: "ตำแหน่งงาน",
      gender: "คำนำหน้า",
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      faculty: "",
      university: "",
    });
  };

  onFileChange = (event) => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] });
  };

  // On file upload (click the upload button)
  onFileUpload = () => {
    // Create an object of formData
    const formData = new FormData();

    // Update the formData object
    formData.append(
      "myFile",
      this.state.selectedFile,
      this.state.selectedFile.name
    );

    // Details of the uploaded file
    console.log(this.state.selectedFile);

    // Request made to the backend api
    // Send formData object
    axios.post("api/uploadfile", formData);
  };

  // File content to be displayed after
  // file upload is complete
  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {this.state.selectedFile.name}</p>
          <p>File Type: {this.state.selectedFile.type}</p>
          <p>
            Last Modified:{" "}
            {this.state.selectedFile.lastModifiedDate.toDateString()}
          </p>
        </div>
      );
    } else {
      return (
        <div>
          <br />
          <h4>Choose before Pressing the Upload button</h4>
        </div>
      );
    }
  };

  render() {
    return (
      <>
        <div className="container">
          <div className="row">
            <div className="col md-3">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/Web-WebApplicationDeveloper.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Web/Web Application Developer</p>
                  <a href="/Job/WebDev" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
            <div className="col md-3">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/SalesManager.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Sales Mamager</p>
                  <a href="/Job/Sale" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
            <div className="col md-3">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/Telesales.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Telesales (พนักงานขายทางโทรศัพท์)</p>
                  <a href="/Job/Tele" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
            <div className="col md-3">
              <div className="card">
                <img
                  className="card-img-top"
                  src="./image/Job/SalesEngineer.jpg"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <p className="card-text">Sales Engineer (Software)</p>
                  <a href="/Job/SaleEn" className="btn btn-primary">
                    สมัครงาน
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
