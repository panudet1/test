import React from "react";
import Link from "next/link";
import { BrowserView, MobileView } from "react-device-detect";
export default function ITsolution() {
  const tex_sty1 = {
    color: "#313131",
    fontSize: "80px",
    fontFamily: "DBAdmanXBd",
  };

  const tex_sty2 = { color: "#313131", fontSize: "25px" };

  return (
    <>
      {" "}
      {/*  */}
      <div className="container mb-5 mt-5">
        <div className="row col-md-12">
          <div className="col-md-6 align-self-center">
            <span className="sup-digital-txt" style={tex_sty1}>
              IT Solution
            </span>
            <ul>
              <div className="list-text mt-4" style={tex_sty2}>
                ให้บริการทางด้าน Software House และ Data Warehouse รวมถึง
                Network Design เพื่อพัฒนาศักยภาพทางด้านไอทีขององค์กร
                ตอบสนองทุกความต้องการของคุณ
              </div>
            </ul>
          </div>
          <div className="col-md-6">
            <img
              className=""
              src="/image/ItSolution/IT Sulotion.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5">
        <div className="row col-md-12">
          <div className="col-md-6">
            <img
              className=""
              src="/image/ItSolution/Software House.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 mt-5">
            <span className="sup-digital-txt " style={tex_sty1}>
              Software House
            </span>
            <ul>
              <div className="list-text mt-4" style={tex_sty2}>
                ออกแบบบริการด้าน IT ปรับปรุงและแก้ไขปัญหาอย่างตรงจุด
                เสริมศักยภาพด้าน IT ให้มีประสิทธิภาพสูงสุด
              </div>
            </ul>
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5 mt-5">
        <div className="row col-md-12">
          <div className="col-md-6 align-self-center">
            <span className="sup-digital-txt" style={tex_sty1}>
              Data Warehouse
            </span>
            <ul>
              <div className="list-text mt-4" style={tex_sty2}>
                ให้บริการออกแบบ
                ติดตั้งและพัฒนาระบบตามมาตรฐานเพื่อการวิเคราะห์ข้อมูลโดยทำการรวบรวมข้อมูลทั้งจากภายใน
                เช่น ระบบบัญชี ระบบบุคคล ระบบขาย และข้อมูลภายนอก
                ตามความต้องการของธุรกิจ
              </div>
            </ul>
          </div>
          <div className="col-md-6">
            <img
              className=""
              src="/image/ItSolution/Data Warehouse.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5">
        <div className="row col-md-12">
          <div className="col-md-6">
            <img
              className=""
              src="/image/ItSolution/Network Design (2).png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 mt-5">
            <span className="sup-digital-txt " style={tex_sty1}>
              Network Design
            </span>
            <ul>
              <div className="list-text mt-4" style={tex_sty2}>
                รับออกแบบ Network Infrastructure เพื่อรองรับเทคโนโลยี 5G และ
                Multiple source ด้วยข้อมูลแบบ Inside และ Exclusive
              </div>
            </ul>
            <div className="list-text text-right" style={tex_sty2}>
              <Link href="/contact">
                <button
                  className="btn btn-contect  mt-1 "
                  style={{ width: "250px" }}
                >
                  Speak to our experts
                </button>
              </Link>
              <Link href="https://line.me/R/ti/p/%40yql4546f">
                <button
                  className="btn btn-contect ml-1  mt-1"
                  style={{ width: "250px", backgroundColor: "#2cc165" }}
                >
                  Add Line
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
