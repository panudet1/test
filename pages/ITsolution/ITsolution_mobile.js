import React from "react";
import { BrowserView, MobileView } from "react-device-detect";
export default function ITsolution() {
  const tex_sty1 = {
    color: "#555555",
    fontSize: "80px",
    fontFamily: "DBAdmanXBd",
  };
  const tex_sty3 = {
    // color: "#555555",
    fontSize: "30px",
    fontFamily: "DBAdmanXBd",
    textAlign: "center",
  };
  const tex_sty2 = { color: "#313131", fontSize: "20px" };
  const tex_sty4 = { color: "#313131", fontSize: "20px", textAlign: "center" };
  return (
    <>
      <div className="container mt-5 ">
        <div className="row ">
          <div className="col">
            <img
              className="mr-4 ml-4"
              src="/image/ItSolution/IT Sulotion.png"
              width="90%"
              height="auto"
              alt=""
            />
          </div>
        </div>
        <div className="row ">
          <div className="col ">
            <div className="" style={tex_sty3}>
              IT Solution
            </div>
            <div className="list-text mr-4 ml-4" style={tex_sty4}>
              ให้บริการทางด้าน Software House และ Data Warehouse รวมถึง Network
              Design เพื่อพัฒนาศักยภาพทางด้านไอทีขององค์กร
              ตอบสนองทุกความต้องการของคุณ
            </div>
          </div>
        </div>
      </div>

      <div className="container  mt-5 ">
        <div className="row ">
          <div className="col">
            <img
              className="mr-4 ml-4"
              src="/image/ItSolution/Software House.png"
              width="90%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="container ">
        <div className="row ">
          <div className=" col ">
            <div className="" style={tex_sty3}>
              Software House
            </div>

            <div className="list-text mr-4 ml-4" style={tex_sty4}>
              ออกแบบบริการด้าน IT ปรับปรุงและแก้ไขปัญหาอย่างตรงจุด
              เสริมศักยภาพด้าน IT ให้มีประสิทธิภาพสูงสุด
            </div>
          </div>
        </div>
      </div>

      <div className="container  mt-5  ">
        <div className="row ">
          <div className="col">
            <img
              className="mr-4 ml-4"
              src="/image/ItSolution/Data Warehouse.png"
              width="90%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="container  ">
        <div className="row ">
          <div className="col align-self-center">
            <div className="col" style={tex_sty3}>
              Data Warehouse
            </div>

            <div className="list-text mr-4 ml-4" style={tex_sty4}>
              ให้บริการออกแบบ
              ติดตั้งและพัฒนาระบบตามมาตรฐานเพื่อการวิเคราะห์ข้อมูลโดยทำการรวบรวมข้อมูลทั้งจากภายใน
              เช่น ระบบบัญชี ระบบบุคคล ระบบขาย และข้อมูลภายนอก
              ตามความต้องการของธุรกิจ
            </div>
          </div>
        </div>
      </div>

      <div className="container mt-5 mb-5">
        <div className="row ">
          <div className="col">
            <img
              className="mr-4 ml-4"
              src="/image/ItSolution/Network Design (2).png"
              width="90%"
              height="auto"
              alt=""
            />
          </div>
        </div>
        <div className="row ">
          <div className="col ">
            <div className="" style={tex_sty3}>
              Network Design
            </div>
            <div className="list-text mr-4 ml-4" style={tex_sty4}>
              รับออกแบบ Network Infrastructure เพื่อรองรับเทคโนโลยี 5G และ
              Multiple source ด้วยข้อมูลแบบ Inside และ Exclusive
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
