import React from "react";
import { BrowserView, MobileView } from "react-device-detect";
export default function pay() {
  return (
    <>
      <BrowserView>
        {" "}
        <div className="container" style={{ display: "flex" }}>
          <div className="container">
            <form
              className="pay mb-3"
              style={{
                backgroundColor: "#68c7ff",
                padding: "20px",
                borderRadius: "19px",
              }}
            >
              <label
                style={{
                  color: "white",
                  fontFamily: "DBAdmanXBd",
                  fontSize: "36px",
                }}
              >
                Billing details
              </label>

              <div className="form-row mb-3">
                <div className="col ">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="First name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Last name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col-md-8">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Company name (optional)"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col-md-4">
                  <select
                    id="inputState"
                    className="form-control"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  >
                    <option disabled selected>
                      Country
                    </option>
                    <option>...</option>
                  </select>
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Street address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Town / City"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <select
                    id="inputState"
                    className="form-control"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  >
                    <option disabled selected>
                      State/Country
                    </option>
                    <option>...</option>
                  </select>
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Postcode / ZIP"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Phone"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Email address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
            </form>

            <label
              style={{
                color: "#328be0",
                fontFamily: "DBAdmanXBd",
                fontSize: "36px",
              }}
            >
              Your order
            </label>
            {/* ///////////// */}
            <div className="container align-self-center">
              <div className="row">
                <div className="col-sm-3">
                  <img
                    src="/image/G-Suite Lifetime License.png"
                    alt="Converse"
                    className="img-product img-responsive img-rounded"
                    width="100%"
                    height="auto"
                  />
                </div>
                <div
                  className="col-sm-3  align-self-center"
                  style={{ color: "#328be0" }}
                >
                  <h4 className="nomargin">ตัวอย่างสินค้า</h4>
                  <p className="hidden-xs">รายละเอี่ยด</p>
                </div>
                <div className="col-sm-2 align-self-center">$60,000.00</div>
                <div className="col-sm-2 align-self-center">
                  {" "}
                  <div className="btn-group" role="group" aria-label="...">
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginRight: "10px",
                      }}
                    >
                      -
                    </button>
                    1
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginLeft: "10px",
                      }}
                    >
                      +
                    </button>
                  </div>
                </div>
                <div
                  className="col-sm-2 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  Total $60,000.00
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div
                  className="col-sm-9 align-self-center"
                  style={{ color: "#328be0" }}
                ></div>
                <div
                  className="col-sm-3 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  Total $60,000.00
                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                      fontFamily: "DBAdmanXBd",
                    }}
                  >
                    Purchase <i className="fa fa-angle-right" />
                  </a>
                </div>
              </div>
            </div>
            <div className="container mb-5">
              <div className="row">
                <div
                  className="col-sm-9 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      โอนผ่านบัญชีธนาคาร
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      ชำระผ่าน PromptPay
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      PayPal
                    </label>
                  </div>
                </div>
                <div
                  className="col-sm-3 align-self-end"
                  style={{ color: "#328be0" }}
                >
                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                      fontFamily: "DBAdmanXBd",
                    }}
                  >
                    Place order
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </BrowserView>
      <MobileView>
        {" "}
        <div className="container" style={{ display: "flex" }}>
          <div className="container">
            <label
              style={{
                color: "#328be0",
                fontFamily: "DBAdmanXBd",
                fontSize: "36px",
              }}
            >
              Your order
            </label>
            <label
              style={{
                color: "#328be0",
                fontFamily: "DBAdmanXBd",
                fontSize: "36px",
              }}
            >
              Your order
            </label>
            {/* <form
              className="pay mb-3"
              style={{
                backgroundColor: "#68c7ff",
                padding: "20px",
                borderRadius: "19px",
              }}
            >
              <label
                style={{
                  color: "white",
                  fontFamily: "DBAdmanXBd",
                  fontSize: "4vw",
                  marginTop: "100pxz",
                }}
              >
                Billing details
              </label>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="First name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Last name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col-md-8">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Company name (optional)"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col-md-4">
                  <select
                    id="inputState"
                    className="form-control"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  >
                    <option disabled selected>
                      Country
                    </option>
                    <option>...</option>
                  </select>
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Street address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Town / City"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <select
                    id="inputState"
                    className="form-control"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  >
                    <option disabled selected>
                      State/Country
                    </option>
                    <option>...</option>
                  </select>
                </div>
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Postcode / ZIP"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Phone"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Email address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "24px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
            </form> */}
            <label
              style={{
                color: "#328be0",
                fontFamily: "DBAdmanXBd",
                fontSize: "36px",
              }}
            >
              Your order
            </label>
            {/* ///////////// */}
            <div className="container align-self-center">
              <div className="row">
                <div className="col-sm-3">
                  <img
                    src="/image/G-Suite Lifetime License.png"
                    alt="Converse"
                    className="img-product img-responsive img-rounded"
                    width="100%"
                    height="auto"
                  />
                </div>
                <div
                  className="col-sm-3  align-self-center"
                  style={{ color: "#328be0" }}
                >
                  <h4 className="nomargin">ตัวอย่างสินค้า</h4>
                  <p className="hidden-xs">รายละเอี่ยด</p>
                </div>
                <div className="col-sm-2 align-self-center">$60,000.00</div>
                <div className="col-sm-2 align-self-center">
                  {" "}
                  <div className="btn-group" role="group" aria-label="...">
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginRight: "10px",
                      }}
                    >
                      -
                    </button>
                    1
                    <button
                      type="button"
                      className="btn btn-sm btn-default"
                      style={{
                        backgroundColor: "#c5c5c5",
                        marginLeft: "10px",
                      }}
                    >
                      +
                    </button>
                  </div>
                </div>
                <div
                  className="col-sm-2 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  Total $60,000.00
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div
                  className="col-sm-9 align-self-center"
                  style={{ color: "#328be0" }}
                ></div>
                <div
                  className="col-sm-3 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  Total $60,000.00
                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                      fontFamily: "DBAdmanXBd",
                    }}
                  >
                    Purchase <i className="fa fa-angle-right" />
                  </a>
                </div>
              </div>
            </div>
            <div className="container mb-5">
              <div className="row">
                <div
                  className="col-sm-9 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      โอนผ่านบัญชีธนาคาร
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      ชำระผ่าน PromptPay
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      PayPal
                    </label>
                  </div>
                </div>
                <div
                  className="col-sm-3 align-self-end"
                  style={{ color: "#328be0" }}
                >
                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                      fontFamily: "DBAdmanXBd",
                    }}
                  >
                    Place order
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MobileView>
    </>
  );
}
