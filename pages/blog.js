import React, { Component } from "react";
import Link from "next/link";
import axios from "axios";
// import Pagination from "react-js-pagination";
import swal from "sweetalert";
export default class blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  handleSubmit = () => {
    var that = this.state;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(that.email)) {
      swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
    } else {
      axios({
        method: "post",
        url: "/api/sendmail",
        data: {
          email: that.email,
        },
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
      this.setState({
        email: "",
      });
      swal("ส่งข้อมูลสำเร็จ", "", "success");
    }
  };

  render() {
    return (
      <>
        <div className="container mb-5">
          <div className="row d-flex justify-content-center">
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInLeftBig ">
                <Link href="/Blog/Phishing_email">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Phishing-Email-600x402.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        ฟิชชิงอีเมล์ (Phishing Email) คืออะไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
            <div className="col-md-1"></div>
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInRightBig">
                {/* <img
                  src="./image/Blog/Email-Server-600x600.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/Email_server">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Email-Server-600x600.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Email Server มีความสำคัญอย่างไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="container mb-5">
          <div className="row d-flex justify-content-center">
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInLeftBig">
                {/* <img
                  src="./image/Blog/Hangout-Meet.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/Work_from_home">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Hangout-Meet.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        การทำงานแบบ Work From Home ด้วย Hangouts Meet
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
            <div className="col-md-1"></div>
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInRightBig">
                {/* <img
                  src="./image/Blog/Google-Meet-800x800.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/Hangouts_meet">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/Google-Meet-800x800.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        Hangouts Meet ประชุมงานได้ทุกที่
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="container mb-5">
          <div className="row d-flex justify-content-center ">
            <div className="col-md-11  ">
              <div className="d-flex flex-column animate__animated animate__flipInX ">
                {/* <img
                  src="./image/Blog/WordPress-01-scaled.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/Word_press">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/WordPress-01-scaled.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        มารู้จักกับ WordPress เครื่องมือสร้างเว็บไซต์
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="container mb-5">
          <div className="row d-flex justify-content-center ">
            <div className="col-md-11  ">
              <div className="d-flex flex-column animate__animated animate__flipInX ">
                {/* <img
                  src="./image/Blog/E-commerce-01-scaled.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/E_commerce">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/E-commerce-01-scaled.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        E-Commerce Website คืออะไร
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="container mb-3">
          <div className="row d-flex justify-content-center ">
            <div className="col-md-11 datail-cen">
              <div
                className="d-flex flex-column  w-70 p-3"
                style={{
                  backgroundColor: " #78bdff",
                  height: "150px",
                  borderRadius: "50px",
                }}
              >
                <div
                  className="p-2"
                  style={{ fontSize: "30px", fontFamily: "DBAdmanXBd" }}
                >
                  อยากให้เราติดต่อกลับโปรดระบุอีเมลล์ของคุณ
                </div>
                <form className="form-inline">
                  <div
                    className="form-group mb-2"
                    style={{
                      backgroundColor: "rgba(255, 255, 255, 0.22)",
                      marginRight: "10px",
                      borderRadius: "50px",
                      paddingLeft: "10px",
                      width: "200px",
                    }}
                  >
                    <input
                      style={{ color: "white", fontFamily: "DBAdmanX" }}
                      type="email"
                      value={this.state.email}
                      onChange={(e) => this.setState({ email: e.target.value })}
                      className="form-control-plaintext"
                      id="staticEmail2"
                      placeholder="Enter your email"
                    />
                  </div>

                  <button
                    type="button"
                    className="btn btn-primary mb-2"
                    onClick={this.handleSubmit}
                    style={{
                      marginRight: "10px",
                      borderRadius: "50px",
                      borderWidth: "0px",
                      paddingLeft: "10px",
                      width: "100px",
                      backgroundColor: " #ffffff",
                      color: "black",
                    }}
                  >
                    Send
                  </button>
                </form>
              </div>
              <img src="/image/mailbox.png" width="300" height="300" alt="" />
            </div>
          </div>
        </div>
        {/* <div>
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={450}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange.bind(this)}
          />
        </div> */}
        <div className="container mb-3 d-flex justify-content-end">
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item active">
                <a className="page-link" href="./blog">
                  1
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_2">
                  2
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_3">
                  3
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog_2">
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}
