import React from "react";
import { BrowserView, MobileView } from "react-device-detect";
export default function pay() {
  return (
    <>
      <MobileView>
        {" "}
        <div className="container" style={{ display: "flex" }}>
          <div className="container">
            <form
              className="pay mb-3"
              style={{
                backgroundColor: "#68c7ff",
                padding: "20px",
                borderRadius: "19px",
                marginTop: "100px",
              }}
            >
              <label
                style={{
                  color: "white",
                  fontFamily: "DBAdmanXBd",
                  fontSize: "30px",
                }}
              >
                Billing details
              </label>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="First name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Last name"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Company name (optional)"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <select
                    id="inputState"
                    className="form-control"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  >
                    <option disabled selected>
                      Country
                    </option>
                    <option>...</option>
                  </select>
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Street address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Town / City"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Postcode / ZIP"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Postcode / ZIP"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>

              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="number"
                    className="form-control"
                    placeholder="Phone"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Email address"
                    style={{
                      borderWidth: "0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "15px",
                      color: "#328be0",
                    }}
                  />
                </div>
              </div>
            </form>
            <label
              style={{
                color: "#328be0",
                fontFamily: "DBAdmanXBd",
                fontSize: "30px",
              }}
            >
              Your order
            </label>
            {/* ///////////// */}
            <table className="table ">
              <thead>
                <tr>
                  <th scope="col">Product</th>
                  <th scope="col">Quantity</th>
                  <th scope="col">Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style={{ width: "40%" }}>
                    <img
                      src="/image/G-Suite Lifetime License.png"
                      width="100%"
                      height="auto"
                    />
                  </td>
                  <td>
                    <button
                      type="button"
                      style={{
                        backgroundColor: "#c5c5c5",
                        textAlign: "center",
                      }}
                    >
                      -
                    </button>
                    1
                    <button
                      type="button"
                      style={{
                        backgroundColor: "#c5c5c5",
                        textAlign: "center",
                      }}
                    >
                      +
                    </button>
                  </td>
                  <td>$60,000.00</td>
                </tr>
              </tbody>

              <tfoot>
                <tr>
                  <td className="text-right">Total</td>
                  <td className="text-right">
                    <strong> $60,000.00</strong>
                  </td>

                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                    }}
                  >
                    Purchase <i className="fa fa-angle-right" />
                  </a>
                </tr>
              </tfoot>
            </table>
            <div className="container mb-5">
              <div className="row">
                <div
                  className="col-sm-9 align-self-center"
                  style={{ color: "#328be0" }}
                >
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      โอนผ่านบัญชีธนาคาร
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      ชำระผ่าน PromptPay
                    </label>
                  </div>
                  <div
                    style={{
                      color: "#328be0",
                      fontFamily: "DBAdmanXBd",
                      fontSize: "36px",
                    }}
                  >
                    <label>
                      <input type="radio" value="1" name="fooby[1][]" />
                      PayPal
                    </label>
                  </div>
                </div>
                <div
                  className="col-sm-3 align-self-end"
                  style={{ color: "#328be0" }}
                >
                  <a
                    href="#"
                    className="btn btn-success btn-block"
                    style={{
                      backgroundColor: "#328be0",
                      borderWidth: "0px",
                      fontFamily: "DBAdmanXBd",
                    }}
                  >
                    Place order
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MobileView>
    </>
  );
}
