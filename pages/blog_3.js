import React, { Component } from "react";
import Link from "next/link";
import swal from "sweetalert";
import axios from "axios";
export default class blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  handleSubmit = () => {
    var that = this.state;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(that.email)) {
      swal("ตรวจสอบอีเมล", "รูปแบบอีเมลไม่ถูกต้อง", "warning");
    } else {
      axios({
        method: "post",
        url: "/api/sendmail",
        data: {
          email: that.email,
        },
      }).then(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log(error);
        }
      );
      this.setState({
        email: "",
      });
      swal("ส่งข้อมูลสำเร็จ", "", "success");
    }
  };
  render() {
    return (
      <>
        <div className="container mb-5">
          <div className="row d-flex justify-content-center">
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInLeftBig ">
                <Link href="/Blog/GSuite">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/G-suite-is-.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        G Suite คืออะไร?
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
            <div className="col-md-1"></div>
            <div className="col-md-5 ">
              <div className="animate__animated animate__fadeInRightBig">
                {/* <img
                  src="./image/Blog/Email-Server-600x600.jpg"
                  width="100%"
                  height=" 400px"
                /> */}
                <Link href="/Blog/5App">
                  <div
                    className="hover hover-2 text-white rounded"
                    style={{ cursor: "pointer" }}
                  >
                    <img src="./image/Blog/5-app.jpg" />
                    <div className="hover-overlay" />
                    <div className="hover-2-content px-5 py-4">
                      <p className="hover-2-description text-uppercase mb-0">
                        รวม 5 แอพเด็ดสำหรับธุรกิจที่ต้อง Work From Home
                      </p>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="container mb-3">
          <div className="row d-flex justify-content-center ">
            <div className="col-md-11 datail-cen">
              <div
                className="d-flex flex-column  w-70 p-3"
                style={{
                  backgroundColor: " #78bdff",
                  height: "150px",
                  borderRadius: "50px",
                }}
              >
                <div
                  className="p-2"
                  style={{ fontSize: "30px", fontFamily: "DBAdmanXBd" }}
                >
                  อยากให้เราติดต่อกลับโปรดระบุอีเมลล์ของคุณ
                </div>
                <form className="form-inline">
                  <div
                    className="form-group mb-2"
                    style={{
                      backgroundColor: "rgba(255, 255, 255, 0.22)",
                      marginRight: "10px",
                      borderRadius: "50px",
                      paddingLeft: "10px",
                      width: "200px",
                    }}
                  >
                    <input
                      style={{ color: "white", fontFamily: "DBAdmanX" }}
                      type="email"
                      className="form-control-plaintext"
                      id="staticEmail2"
                      placeholder="Enter your email"
                      value={this.state.email}
                      onChange={(e) => this.setState({ email: e.target.value })}
                    />
                  </div>

                  <button
                    type="button"
                    className="btn btn-primary mb-2"
                    onClick={this.handleSubmit}
                    style={{
                      marginRight: "10px",
                      borderRadius: "50px",
                      borderWidth: "0px",
                      paddingLeft: "10px",
                      width: "100px",
                      backgroundColor: " #ffffff",
                      color: "black",
                    }}
                  >
                    Send
                  </button>
                </form>
              </div>
              <img src="/image/mailbox.png" width="300" height="300" alt="" />
            </div>
          </div>
        </div>

        <div className="container mb-3 d-flex justify-content-end">
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item">
                <a className="page-link" href="./blog_2">
                  Previous
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="./blog">
                  1
                </a>
              </li>
              <li className="page-item ">
                <a className="page-link" href="./blog_2">
                  2
                </a>
              </li>
              <li className="page-item active">
                <a className="page-link" href="./blog_3">
                  3
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </>
    );
  }
}
