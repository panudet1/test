import React from "react";
import Link from "next/link";
import { BrowserView, MobileView } from "react-device-detect";
export default function shop() {
  return (
    <>
      <div style={{ height: "50px" }}></div>
      <div className="container">
        <table className="table ">
          <thead>
            <tr>
              <th scope="col">Product</th>
              <th scope="col">Quantity</th>
              <th scope="col">Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style={{ width: "40%" }}>
                <img
                  src="/image/G-Suite Lifetime License.png"
                  width="100%"
                  height="auto"
                />
              </td>
              <td>
                <button
                  type="button"
                  style={{
                    backgroundColor: "#c5c5c5",
                    textAlign: "center",
                  }}
                >
                  -
                </button>
                1
                <button
                  type="button"
                  style={{
                    backgroundColor: "#c5c5c5",
                    textAlign: "center",
                  }}
                >
                  +
                </button>
              </td>
              <td>$60,000.00</td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td className="text-right">Total</td>
              <td className="text-right">
                <strong> $60,000.00</strong>
              </td>
              <Link href="/pay_mobile">
                <a
                  href="#"
                  className="btn btn-success btn-block"
                  style={{
                    backgroundColor: "#328be0",
                    borderWidth: "0px",
                  }}
                >
                  Purchase <i className="fa fa-angle-right" />
                </a>
              </Link>
            </tr>
          </tfoot>
        </table>
      </div>
    </>
  );
}
