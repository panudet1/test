import React, { Component } from "react";
import { BrowserView, MobileView, isMobile } from "react-device-detect";
import ReactPlayer from "react-player";
import Link from "next/link";

const tex_sty_mobile = { color: "#313131", fontSize: "15px" };
const tex_sty_mobile_price = { color: "#f74646", fontSize: "2vw" };
const tex_sty2 = { color: "#313131", fontSize: "25px" };

export default class motion extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5 ">
          <div className="row">
            <div className="col-md-6">
              <ReactPlayer
                className="react-player"
                width="100%"
                height="100%"
                url="/Videos/MG3.mp4"
                controls={true}
              />
            </div>
            <div className="col-md-6 mt-5">
              <div className="list-text text-right" style={tex_sty2}>
                เปลี่ยนภาพ content ธรรมดาให้เป็นภาพ VDO ที่น่าชม ด้วย Motion
                Graphic
              </div>
              <div
                className="list-text text-right"
                style={tex_sty_mobile_price}
              >
                ราคาเริ่มต้น 2,000 บาท
              </div>
              <div className="list-text text-right" style={tex_sty2}>
                <Link href="/contact">
                  <button
                    className="btn btn-contect  mt-1"
                    style={{ width: "250px" }}
                  >
                    Speak to our experts
                  </button>
                </Link>
                <Link href="https://line.me/R/ti/p/%40yql4546f">
                  <button
                    className="btn btn-contect  ml-1 mt-1"
                    style={{ width: "250px", backgroundColor: "#2cc165" }}
                  >
                    Add Line
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
