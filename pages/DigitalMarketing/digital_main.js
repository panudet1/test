import React from "react";
import { Tabs, Tab, Panel } from "@bumaga/tabs";
import Barner from "./barner";
import Info from "./info";
import Motion from "./motion";
import Link from "next/link";
export default function digtal_main() {
  const tex_sty2 = { color: "#313131", fontSize: "25px" };
  const tex_sty3 = {
    fontSize: "40px",
    color: "#f74646",
    fontFamily: "DBAdmanXBd",
  };

  return (
    <>
      <div className="container mb-5" style={{ marginTop: "144px" }}>
        <div className="row col-md-12">
          <div className="col-md-6 align-self-center">
            <img
              className=""
              src="/image/DigitalMar/Marketing1.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 mt-5">
            <span className="sup-digital-txt d-flex justify-content-end">
              <b style={{ marginRight: "15px" }}>Digital</b> Marketing
            </span>
            <div className="list-text mt-4 text-right" style={tex_sty2}>
              Gold Opportunity Of Sales Channels สร้างยอดขาย
              เพิ่มกำไรด้วยช่องทางการขายยอดนิยม
              สร้างจุดแข็งที่เหนือกว่าคู่แข่งของแบรนด์
              ลดต้นทุนและค่าใช้จ่ายต่างๆ เปิดธุรกิจให้บริการ
              เพื่อสร้างยอดขายได้ตลอดเวลา 24/7
            </div>
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5" style={{ marginTop: "230px" }}>
        <div className="row col-md-12">
          <div className="col-md-6 align-self-center">
            <span className="sup-digital-txt">
              <b>รับจัดทำ Website</b>
            </span>
            <div className="list-text mt-4" style={tex_sty2}>
              รับจัดทำเว็บไซต์สำหรับองค์กร เว็บไซต์ทั่วไป
              เพื่อใช้เป็นสื่อกลางในการให้ข้อมูลต่างๆ
              และช่วยให้ลูกค้าเข้าถึงแบรนด์หรือธุรกิจของคุณได้ง่ายขึ้น
              รวมถึงเว็บไซต์สำหรับธุรกิจอีคอมเมิร์ซ
            </div>
            <div className="list-text mt-4" style={tex_sty3}>
              เริ่มต้น 30,000 บาท
            </div>
          </div>
          <div className="col-md-6">
            <img
              className=""
              src="/image/DigitalMar/Marketing2.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5" style={{ marginTop: "230px" }}>
        <div className="row col-md-12">
          <div className="col-md-6">
            <img
              className=""
              src="/image/DigitalMar/Marketing3.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 align-self-center">
            <span className="sup-digital-txt d-flex justify-content-end">
              <b>Line Marketing</b>
            </span>

            <div className="list-text mt-4 text-right" style={tex_sty2}>
              ช่วยให้คุณใกล้ชิดลูกค้ามากขึ้นด้วยสื่อ Line
              สร้างโอกาสขายและความประทับใจให้ลูกค้าด้วยระบบทักทายอัตโนมัติ
              สื่อสารข้อมูลหรือประชาสัมพันธ์โปรโมชั่นของคุณถึงลูกค้าในครั้งเดียว
            </div>
            <div className="list-text mt-4  text-right" style={tex_sty3}>
              เริ่มต้น 10,000 บาท
            </div>
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5" style={{ marginTop: "230px" }}>
        <div className="row col-md-12">
          <div className="col-md-8  align-self-center">
            <span className="sup-digital-txt">
              <b>Facebook Marketing&</b>
              <br /> <b>Psychographic Marketing</b>
            </span>
            {/* <span className="sup-digital-txt">
             
            </span> */}
            <div className="list-text " style={tex_sty2}>
              รับจัดทำเว็บไซต์สำหรับองค์กร เว็บไซต์ทั่วไป
              เพื่อใช้เป็นสื่อกลางในการให้ข้อมูลต่างๆ
              และช่วยให้ลูกค้าเข้าถึงแบรนด์หรือธุรกิจของคุณได้ง่ายขึ้น
              รวมถึงเว็บไซต์สำหรับธุรกิจอีคอมเมิร์ซ
            </div>
            <div className="list-text mt-4" style={tex_sty3}>
              เริ่มต้น 30,000 บาท
            </div>
          </div>
          <div className="col-md-4 align-self-center">
            <img
              className=""
              src="/image/DigitalMar/Marketing4.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
        </div>
      </div>
      {/*  */}
      <div className="container mb-5" style={{ marginTop: "230px" }}>
        <div className="row col-md-12">
          <div className="col-md-6">
            <img
              className=""
              src="/image/DigitalMar/VECTOR-Graphic.png"
              width="100%"
              height="auto"
              alt=""
            />
          </div>
          <div className="col-md-6 mt-5">
            <span className="sup-digital-txt d-flex justify-content-end">
              <b style={{ marginRight: "15px" }}>Graphic </b> Design
            </span>
            <div
              className="list-text text-right"
              style={{ color: "#313131", fontSize: "40px" }}
            >
              “น่าดู ดึงดูด ได้ใจ”
            </div>
            <div className="list-text text-right" style={tex_sty2}>
              สร้างคอนเทนต์ที่ดี ด้วยสื่อที่น่าสนใจ สร้างความหลากหลาย
              และสะกดใจลูกค้าให้ กดสั่งซื้อสินค้า
            </div>
          </div>
        </div>
      </div>

      <div className="container mb-5 " style={{ marginTop: "171px" }}>
        <Tabs>
          <div className=" d-flex justify-content-center">
            <Tab>
              <button
                autoFocus
                className="btn btn-contect mr-1 btn_tab_info"
                style={{ width: "250px" }}
              >
                Infographic
              </button>
            </Tab>
            <Tab>
              <button
                className="btn btn-contect btn_tab_info"
                style={{ width: "250px" }}
              >
                Banner/Ads โฆษณา
              </button>
            </Tab>
            <Tab>
              <button
                className="btn btn-contect  ml-1 btn_tab_info"
                style={{ width: "250px" }}
              >
                Motion Graphic
              </button>
            </Tab>
          </div>

          <Panel>
            <Info />
          </Panel>
          <Panel>
            <Barner />
          </Panel>
          <Panel>
            <Motion />
          </Panel>
        </Tabs>
      </div>
    </>
  );
}
