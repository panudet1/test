import React, { Component } from "react";
import Link from "next/link";
import { BrowserView, MobileView, isMobile } from "react-device-detect";

const tex_sty2 = { color: "#313131", fontSize: "25px" };
const tex_sty_mobile = { color: "#313131", fontSize: "4vw" };
const tex_sty_mobile_price = { color: "#f74646", fontSize: "4vw" };

export default class info_m extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5 ">
          <div className="row md-12">
            <div className="col md-6">
              <img
                className=""
                src="/image/DigitalMar/IF2-400x400.jpg"
                width="100%"
                height="auto"
                alt=""
              />
            </div>
            <div className="col md-6">
              <div className="list-text text-right" style={tex_sty_mobile}>
                รับทำภาพสื่อ Infographic เพื่ออธิบายหรือเล่าเรื่องราวต่างๆ
                ภาพหนึ่งภาพเล่าเรื่องราวได้มากกว่าที่คิด
              </div>
              <div
                className="list-text text-right"
                style={tex_sty_mobile_price}
              >
                ราคาเริ่มต้น 1,000 บาท
              </div>
              <div className="list-text text-right" style={tex_sty_mobile}>
                <Link href="/contact_mobile">
                  <button
                    className="btn btn-contect"
                    style={{ width: "100%", fontSize: "4vw" }}
                  >
                    Speak to our experts
                  </button>
                </Link>
              </div>
              <div className="list-text text-right mt-2" style={tex_sty_mobile}>
                <Link href="https://line.me/R/ti/p/%40yql4546f">
                  <button
                    className="btn btn-contect"
                    style={{
                      width: "100%",
                      backgroundColor: "#2cc165",
                      fontSize: "4vw",
                    }}
                  >
                    Add Line
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
