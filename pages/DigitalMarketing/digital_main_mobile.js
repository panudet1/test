import React from "react";
import { Tabs, Tab, Panel } from "@bumaga/tabs";
import Barner from "./barner_m";
import Info from "./info_m";
import Motion from "./motion_m";
import { BrowserView, MobileView } from "react-device-detect";
import Link from "next/link";
export default function digtal_main_mobile() {
  const tex_sty1 = {
    color: "#555555",
    fontSize: "80px",
    fontFamily: "DBAdmanXBd",
  };
  const tex_sty2 = { color: "#313131", fontSize: "25px" };
  const tex_sty_mobile = { color: "#313131", fontSize: "20px" };
  const tex_sty3 = {
    fontSize: "20px",
    color: "#f74646",
    fontFamily: "DBAdmanXBd",
    textAlign: "center",
  };

  return (
    <>
      <div style={{ width: "90%", marginTop: "100px" }}></div>
      <div className="container ">
        <div className="col align-self-center">
          <img
            className=""
            src="/image/DigitalMar/Marketing1.png"
            width="100%"
            height="auto"
            alt=""
          />
        </div>
        <div className="row ">
          <div className="col">
            <div
              style={{
                fontSize: "30px",
                marginTop: "10px",
                textAlign: "center",
              }}
            >
              Digital Marketing
            </div>
            <div
              className="list-text text-center  mr-4 ml-4"
              style={tex_sty_mobile}
            >
              Gold Opportunity Of Sales Channels สร้างยอดขาย
              เพิ่มกำไรด้วยช่องทางการขายยอดนิยม
              สร้างจุดแข็งที่เหนือกว่าคู่แข่งของแบรนด์
              ลดต้นทุนและค่าใช้จ่ายต่างๆ เปิดธุรกิจให้บริการ
              เพื่อสร้างยอดขายได้ตลอดเวลา 24/7
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-5">
        <div className="col align-self-center">
          <img
            className=""
            src="/image/DigitalMar/Marketing2.png"
            width="100%"
            height="auto"
            alt=""
          />
        </div>
        <div className="row">
          <div className="col">
            <div
              style={{
                fontSize: "30px",
                marginTop: "10px",
                textAlign: "center",
              }}
            >
              รับจัดทำ Website
            </div>
            <div
              className="list-text  text-center   mr-4 ml-4"
              style={tex_sty_mobile}
            >
              รับจัดทำเว็บไซต์สำหรับองค์กร เว็บไซต์ทั่วไป
              เพื่อใช้เป็นสื่อกลางในการให้ข้อมูลต่างๆ
              และช่วยให้ลูกค้าเข้าถึงแบรนด์หรือธุรกิจของคุณได้ง่ายขึ้น
              รวมถึงเว็บไซต์สำหรับธุรกิจอีคอมเมิร์ซ
            </div>
            <div className="list-text  " style={tex_sty3}>
              เริ่มต้น 30,000 บาท
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-5">
        <div className="col align-self-center">
          <img
            className=""
            src="/image/DigitalMar/Marketing3.png"
            width="100%"
            height="auto"
            alt=""
          />
        </div>
        <div className="row">
          <div className="col">
            <div
              style={{
                fontSize: "30px",
                marginTop: "10px",
                textAlign: "center",
              }}
            >
              Line Marketing
            </div>
            <div
              className="list-text text-center   mr-4 ml-4"
              style={tex_sty_mobile}
            >
              ช่วยให้คุณใกล้ชิดลูกค้ามากขึ้นด้วยสื่อ Line
              สร้างโอกาสขายและความประทับใจให้ลูกค้าด้วยระบบทักทายอัตโนมัติ
              สื่อสารข้อมูลหรือประชาสัมพันธ์โปรโมชั่น ของคุณถึงลูกค้าในครั้งเดียว
            </div>
            <div className="list-text" style={tex_sty3}>
              เริ่มต้น 10,000 บาท
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-5">
        <div className="col align-self-center">
          <img
            className=""
            src="/image/DigitalMar/Marketing4.png"
            width="100%"
            height="auto"
            alt=""
          />
        </div>
        <div className="row">
          <div className="col">
            <div
              style={{
                fontSize: "30px",
                marginTop: "10px",
                textAlign: "center",
              }}
            >
              Facebook Marketing
              <br />
              &Psychographic Marketing
            </div>
            <div
              className="list-text text-center   mr-4 ml-4"
              style={tex_sty_mobile}
            >
              รับจัดทำเว็บไซต์สำหรับองค์กร เว็บไซต์ทั่วไป
              เพื่อใช้เป็นสื่อกลางในการให้ข้อมูลต่างๆ
              และช่วยให้ลูกค้าเข้าถึงแบรนด์หรือธุรกิจของคุณได้ง่ายขึ้น
              รวมถึงเว็บไซต์สำหรับธุรกิจอีคอมเมิร์ซ
            </div>
            <div className="list-text " style={tex_sty3}>
              เริ่มต้น 30,000 บาท
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-5">
        <div className="col align-self-center">
          <img
            className=""
            src="/image/DigitalMar/VECTOR-Graphic.png"
            width="100%"
            height="auto"
            alt=""
          />
        </div>
        <div className="row">
          <div className="col">
            <div
              style={{
                fontSize: "30px",
                marginTop: "10px",
                textAlign: "center",
              }}
            >
              {" "}
              Graphic Design
            </div>
            <div
              className="list-text text-right text-center"
              style={{ color: "#313131", fontSize: "20px" }}
            >
              “น่าดู ดึงดูด ได้ใจ”
            </div>
            <div
              className="list-text text-center mr-4 ml-4"
              style={tex_sty_mobile}
            >
              สร้างคอนเทนต์ที่ดี ด้วยสื่อที่น่าสนใจ สร้างความหลากหลาย
              และสะกดใจลูกค้าให้ กดสั่งซื้อสินค้า
            </div>

            <div
              className="list-text text-right mb-2  d-flex justify-content-center"
              style={tex_sty2}
            >
              {/* <Link href="/contact_mobile">
                <button
                  className="btn btn-contect "
                  style={{ width: "30%", fontSize: "10px" }}
                >
                  Speak to our experts
                </button>
              </Link>
              <Link href="https://line.me/R/ti/p/%40yql4546f">
                <button
                  className="btn btn-contect ml-2"
                  style={{
                    width: "30%",
                    backgroundColor: "#2cc165",
                    fontSize: "10px",
                  }}
                >
                  Add Line
                </button>
              </Link> */}
            </div>
          </div>
        </div>
      </div>

      <div className="container mb-5 mt-5 ">
        <Tabs>
          <div className=" d-flex justify-content-center">
            <Tab>
              <button
                // autoFocus
                className="btn btn-contect mr-1 btn_tab_info"
                style={{ width: "250px", fontSize: "20px" }}
              >
                Infographic
              </button>
            </Tab>
            <Tab>
              <button
                className="btn btn-contect btn_tab_info"
                style={{ width: "250px", fontSize: "20px" }}
              >
                Banner/Ads โฆษณา
              </button>
            </Tab>
            <Tab>
              <button
                className="btn btn-contect  ml-1 btn_tab_info"
                style={{ width: "250px", fontSize: "20px" }}
              >
                Motion Graphic
              </button>
            </Tab>
          </div>

          <Panel>
            <Info />
          </Panel>
          <Panel>
            <Barner />
          </Panel>
          <Panel>
            <Motion />
          </Panel>
        </Tabs>
      </div>
    </>
  );
}
