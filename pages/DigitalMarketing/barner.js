import React, { Component } from "react";
import { BrowserView, MobileView, isMobile } from "react-device-detect";
import Link from "next/link";

const tex_sty_mobile = { color: "#313131", fontSize: "15px" };
const tex_sty_mobile_price = { color: "#f74646", fontSize: "2vw" };
const tex_sty2 = { color: "#313131", fontSize: "25px" };

export default class barner extends Component {
  render() {
    return (
      <>
        <div className="container mb-5 mt-5 ">
          <div className="row">
            <div className="col-md-6">
              <img
                className=""
                src="/image/DigitalMar/ADS3-400x400.jpg"
                width="100%"
                height="auto"
                alt=""
              />
            </div>
            <div className="col-md-6 mt-5">
              <div className="list-text text-right" style={tex_sty2}>
                สร้าง Post content หรือโฆษณาด้วยภาพสวยๆ
                เพื่อดึงดูดความสนใจของลูกค้า ให้ลูกค้าสนใจสินค้าหรือบริการของคุณ
              </div>
              <div
                className="list-text text-right"
                style={tex_sty_mobile_price}
              >
                ราคาเริ่มต้น 500 บาท
              </div>
              <div className="list-text text-right" style={tex_sty2}>
                <Link href="/contact">
                  <button
                    className="btn btn-contect mt-1"
                    style={{ width: "250px" }}
                  >
                    Speak to our experts
                  </button>
                </Link>
                <Link href="https://line.me/R/ti/p/%40yql4546f">
                  <button
                    className="btn btn-contect ml-1 mt-1"
                    style={{ width: "250px", backgroundColor: "#2cc165" }}
                  >
                    Add Line
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
