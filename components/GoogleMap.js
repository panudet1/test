import React, { Component } from "react";
import GoogleMapReact from "google-map-react";

const AnyReactComponent = ({ text }) => <div>{text}</div>;

class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 13.88,
      lng: 100.47,
    },
    zoom: 11,
  };

  render() {
    return (
      // Important! Always set the container height explicitly  13.889161838405451, 100.47684752327396
      <div style={{ height: "50vh", width: "50%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyCn8dsAAcyrWbu_hb1y3xC-XIwyu1tkWjk" }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={13.889161}
            lng={100.4768475}
            text="My Marker"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;
