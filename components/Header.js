import Link from "next/link";
import { slide as Menu } from "react-burger-menu";
import { isMobile, isMobileOnly, isTablet } from "react-device-detect";
import React, { Component } from "react";
const style = {
  color: "#595959",
  textAlign: "center",
  textDecoration: "none",
  display: "inline-block",
  fontSize: "2vw",
  cursor: "pointer",
};
const style2 = {
  color: "#595959",
  padding: "10px 10px",
  textAlign: "center",
  textDecoration: "none",
  display: "inline-block",
  fontSize: "20px",
  margin: "4px 2px",
  cursor: "pointer",
};
const style3 = {
  color: "#000000",
  textAlign: "center",
  textDecoration: "none",
  display: "inline-block",
  fontSize: "30px",
  cursor: "pointer",
  fontFamily: "DBAdmanX",
};
export default class Header extends Component {
  renderContent = () => {
    if (isMobile) {
      // if (isTablet) {
      //   var w = "90%";
      // alert(window.innerWidth);
      if (window.innerWidth <= 480) {
        var w = "90%";
      } else {
        var w = "50%";
      }
      return (
        <>
          <div
            className="style_box"
            style={{
              width: "100%",
              height: "50px",
              backgroundColor: "#ffffff",
              left: "0px",
              top: "0px",
              position: "fixed",
              zIndex: "100",
            }}
          >
            <Menu width={w}>
              <div
                className="row  d-flex justify-content-center   "
                style={{
                  outline: "none",
                }}
              >
                <img
                  src="/image/Logo.png"
                  width="150px"
                  height="150px"
                  alt=""
                />
              </div>
              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a id="home" className="menu-item" style={style3} href="/">
                  Home
                </a>
              </div>
              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a
                  id="about"
                  className="menu-item"
                  style={style3}
                  href="/DigitalMarketing/digital_main_mobile"
                >
                  Digital&Marketing
                </a>
              </div>
              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a
                  id="contact"
                  className="menu-item"
                  style={style3}
                  href="/ITsolution/ITsolution_mobile"
                >
                  IT Solution
                </a>
              </div>
              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a
                  className="menu-item--small"
                  href="/blog_mobile"
                  style={style3}
                >
                  Blog
                </a>
              </div>
              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a
                  className="menu-item--small"
                  href="/apply_job_m"
                  style={style3}
                >
                  สมัครงาน
                </a>
              </div>

              <div
                className="row pl-3 ml-1 mr-1"
                style={{
                  outline: "none",
                }}
              >
                <a
                  className="menu-item--small on_outline"
                  href="/contact_mobile"
                  style={style3}
                >
                  Contact Us
                </a>
              </div>
            </Menu>
          </div>
        </>
      );
    } else {
      return (
        <>
          {" "}
          <nav className="container " style={{ marginTop: "5px" }}>
            <div className="row d-flex justify-content-center">
              <div className="col d-flex align-items-center">
                <img src="/image/Logo.png" width="120%" height="auto" alt="" />
              </div>
              <div className="col d-flex align-items-center">
                <Link href="/">
                  <a style={style}>Home</a>
                </Link>
              </div>
              <div className="col d-flex align-items-center">
                <div className="dropdown">
                  <a style={style}>Service</a>
                  <div className="dropdown-content">
                    <Link href="/DigitalMarketing/digital_main">
                      <a style={style2}>Digital&Marketing</a>
                    </Link>
                    <Link href="/ITsolution/ITsolution">
                      <a style={style2}>IT Solution</a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col d-flex align-items-center">
                <Link href="/blog">
                  <a style={style}>Blog</a>
                </Link>
              </div>
              <div className="col d-flex align-items-center">
                <Link href="/apply_job">
                  <a style={style}>สมัครงาน</a>
                </Link>
              </div>
              <div className="col d-flex align-items-center">
                <Link href="/contact">
                  <button
                    type="button"
                    className="btn btn-contect-head  d-flex align-items-center"
                    style={{
                      fontSize: "2vw",
                      width: "90%",
                      height: "50%",
                    }}
                  >
                    Contact Us
                  </button>
                </Link>
              </div>
            </div>
          </nav>
        </>
      );
    }
  };

  render() {
    return this.renderContent();
  }
}
