import Link from "next/link";
const style = {
  right: 0,
  bottom: 0,
  left: 0,
  fontSize: "18px",
  padding: "2.4rem",
  backgroundColor: "#efefef",
  // position: "fixed",
};
const style2 = {
  color: "#595959",
  textAlign: "center",
  textDecoration: "none",
  display: "inline-block",
  fontSize: "2vw",
  cursor: "pointer",
};

const Footer = () => (
  <div style={style}>
    <div className="row d-flex align-items-center">
      <div className="col-md-7 col-lg-8">
        <p className="text-center text-md-left">
          © Copyright 2020 Tectony Co.,Ltd
        </p>
      </div>
      <div className="col-md-5 col-lg-4 ml-lg-0">
        <div className="text-center text-md-right">
          <ul className="list-unstyled list-inline">
            <Link href="https://www.facebook.com/TecTonyMarketing">
              <li className="list-inline-item">
                <img
                  style={style2}
                  src="/image/icon/facebook.png"
                  width="35"
                  height="35"
                />
              </li>
            </Link>
            <Link href="https://line.me/R/ti/p/%40yql4546f">
              <li className="list-inline-item">
                <img
                  style={style2}
                  className="ml-4"
                  src="/image/icon/line.png"
                  width="35"
                  height="35"
                />
              </li>
            </Link>
            {/* <Link href="https://www.youtube.com/channel/UCFLdKCxbuyC-Ue2IoiMEsYQ">
              <li className="list-inline-item">
                <img
                  style={style2}
                  className="ml-4"
                  src="/image/icon/youtube.png"
                  width="35"
                  height="35"
                />
              </li>
            </Link> */}
            <Link href="mailto:info@tectony.co.th">
              <li className="list-inline-item">
                <img
                  style={style2}
                  className="ml-4"
                  src="/image/icon/mail.png"
                  width="35"
                  height="35"
                />
              </li>
            </Link>
          </ul>
        </div>
      </div>
    </div>
  </div>
);

export default Footer;
