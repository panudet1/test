const mongoose = require('mongoose');

const RegisterSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true,'Please add a title'],
        maxlength: [10,'Title cannot be more than 40 characters']
    },
    name: {
        type: String,
        required: [true,'Please add a title'],
        maxlength: [300,'Title cannot be more than 40 characters']
    },
    email: {
        type: String,
        maxlength: [100,'Title cannot be more than 40 characters']
    },
    tel: {
        type: String,
        maxlength: [10,'Title cannot be more than 40 characters']
    },
    resumefile: {
        type: String,
        maxlength: [300,'Title cannot be more than 40 characters']
    },
    type: {
        type: String,
        maxlength: [80,'Title cannot be more than 40 characters']
    },
    branch: {
        type: String,
        maxlength: [80,'Title cannot be more than 40 characters']
    },
    university: {
        type: String,
        maxlength: [80,'Title cannot be more than 40 characters']
    },
    date: {
        type: String,
        maxlength: [50,'Title cannot be more than 40 characters']
    }
})
module.exports = mongoose.model.Register || mongoose.model('Register' ,RegisterSchema);